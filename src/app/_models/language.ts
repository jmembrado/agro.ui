export class Language {

    IdLanguage: number;
    Code: string;
    Name: string;
}