export class Product {
	IdProduct: number;
	Incode?: string;
	Diameter?: number;
	Width?: number;
	Height?: number;
	Thikness?: number;
	Weight?: number;
	UnitPacking?: number;
	Comment?: string;
	IdPacking: number;
	IdFormatLength: number;
	IdFormatWeight: number;

	ProductLanguage: ProductLanguage[];
}

export class ProductLanguage {
	IdProduct: number;
	IdLanguage: number;
	Description?: string;
}