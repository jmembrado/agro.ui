export class Country {
        IdCountry: number;
        Code1: string;
        Code2: string;

        IdRegionNavigation: Region;
        CountryLanguage: CountryLanguage[];
}

export class Region {
        IdRegion: number;
        RegionLanguage: RegionLanguage[];
}

export class CountryLanguage {
	IdLanguage: number;
	IdCountry: number;
        Name: string;
        NameISO: string;
}

export class RegionLanguage {
	IdLanguage: number;
	IdRegion: number;
        Description: string;
}