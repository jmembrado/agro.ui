export class Proyect {
	IdProyect: number;
	Date: Date;
	Priority: number;
	IdEntity: number;
	ProyectLanguage: ProyectLanguage[];
}

export class ProyectLanguage {
	IdProyect: number;
	IdLanguage: number;
	Name?: string;
	Description?: string;
}