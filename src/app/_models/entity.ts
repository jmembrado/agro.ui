import { Country } from './country';

export class Entity {
    
        IdEntity: number;
        Name: string;
        SocialName: string;
        Doc: string;
        Phone1: string;
        Phone2: string;
        IdCountry: number;

        Country: Country;
    }