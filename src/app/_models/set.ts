export class Set {
    IdSet: number;
    IdProject: number;
    IdTemplate: number;
	Sys: string;
    Sys2: string;
    Sec: string;
    Sec2: string;
    Priority: number;
    Comment:string;
    Date: Date;
    EndDate: Date;
    IsOpen: boolean;
	SetLanguage: SetLanguage[];
}

export class SetLanguage {
	IdSet: number;
	IdLanguage: number;
	Name?: string;
}