export class TranslationRequest {
    IdSourceLanguage: number;
    Text: string;
    DestLanguages: number[];
    Prop: string;
}

export class TranslationResponse {
    IdLanguage: number;
    Success: boolean;
    Text: string;
    Prop: string;
    ErrorMessage: string;
}