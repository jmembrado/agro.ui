
import { Country } from './country';

export class Address {
    IdEntity: number;
    IdAddress: number;
    Line1: string;
    Line2: string;
    Comment: string;
    IdCountry: number;
    Type: string;

    IdCountryNavigation: Country;
}