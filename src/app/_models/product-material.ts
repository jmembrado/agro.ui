import {Material} from './material';

export class ProductMaterial {
	IdProduct: number;
	IdMaterial: number;
    Percentage?: number;
    
    Material: Material[];
}