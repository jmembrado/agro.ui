export class MaterialLanguage {
	IdMaterial: number;
	IdLanguage: number;
	Description?: string;
}

export class Material {
    IdMaterial: number;
	
    MaterialLanguage: MaterialLanguage[];
}

