export class Attach {
    IdAttach: number;
    IdSource: number;
    Title: string;
    Data: any;
    TypeOf: string;
    Extension: string;
}