export class Template {
	IdTemplate: number;
	Sys: string;
    Sys2: string;
	TemplateLanguage: TemplateLanguage[];
}

export class TemplateLanguage {
	IdTemplate: number;
	IdLanguage: number;
	Name?: string;
}