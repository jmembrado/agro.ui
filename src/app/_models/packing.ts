export class Packing {
    IdPacking: number;
    
    PackingLanguage: PackingLanguage[];
}
 
export class PackingLanguage {
	IdPacking: number;
	IdLanguage: number;
	Description?: string;
}