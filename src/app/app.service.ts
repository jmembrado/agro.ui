﻿import { Component, Input, Injectable } from '@angular/core';
import { Headers } from "@angular/http";

@Injectable()
export class AppService {
  
    private isLoaded: boolean = false;
    public defaultLanguage = 1;

    constructor() {
        if (this.isLoaded)
            return;

        this.isLoaded = true;
    }

    public loadFromStorage(valueName: string, fromJSON: boolean): any
    {
        let sValue = sessionStorage.getItem(valueName);
        
        if (sValue != null && sValue.length > 0)
        {
            if (fromJSON)
                return JSON.parse(sessionStorage.getItem(valueName));
            else
                return sessionStorage.getItem(valueName);
        }
        return null;
    }
}