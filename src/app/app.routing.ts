import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent, CanActivateViaAuthGuard } from './auth'

import { AttachListComponent } from './attach'
import { AddressListComponent, AddressDetailDefinitionComponent } from './address'
import { DashboardComponent } from './dashboard/dashboard.component'
import { EntityListComponent, EntityDetailComponent, EntityDetailDefinitionComponent } from './entity'
import { MaterialListComponent, MaterialDetailComponent } from './material'
import { PackingListComponent, PackingDetailComponent } from './packing'
import { ProjectListComponent, ProjectDetailDefinitionComponent } from './project'
import { ProductListComponent, ProductDetailComponent } from './product'
import { SetListComponent, SetDetailComponent } from './set'
import { TemplateListComponent, TemplateTreeEditorComponent, TemplateDetailDefinitionComponent } from './template'

import { OrderComponent } from './order/order.component'
import { ShippingComponent } from './shipping/shipping.component'

// Import Containers
import { FullLayout } from './_containers';

export const routes: Routes = [
  
  { path: '', redirectTo: '/login', pathMatch: 'full', },
  { path: 'login', component:  AuthComponent},
  { path: 'admin', component: FullLayout, 
     children: [
        { path: '', redirectTo: "/dashboard", pathMatch: 'full' },
        { path: 'dashboard', component: DashboardComponent},
        { path: 'entity/:type', component: EntityListComponent},
        { path: 'entity/:type/details', component: EntityDetailDefinitionComponent},
        { path: 'entity/:type/details/:id', component: EntityDetailComponent,
          children: [
            { path: '', redirectTo: "definition", pathMatch: 'full'},
            { path: 'definition', component: EntityDetailDefinitionComponent},
            { path: 'address', component: AddressListComponent},
            { path: 'address/details', component: AddressDetailDefinitionComponent},
            { path: 'address/details/:ida', component: AddressDetailDefinitionComponent},
          ]
        },
        { path: 'materials', component: MaterialListComponent},
        { path: 'materials/details', component: MaterialDetailComponent},
        { path: 'materials/details/:idm', component: MaterialDetailComponent},
        { path: 'packings', component: PackingListComponent},
        { path: 'packings/details', component: PackingDetailComponent},
        { path: 'packings/details/:idp', component: PackingDetailComponent},
        { path: 'products', component: ProductListComponent},
        { path: 'products/details', component: ProductDetailComponent},
        { path: 'products/details/:idpr', component: ProductDetailComponent},
        { path: 'templates', component: TemplateListComponent},
        { path: 'templatestree', component: TemplateTreeEditorComponent},
        { path: 'templates/details', component: TemplateDetailDefinitionComponent},
        { path: 'templates/details/:idt', component: TemplateDetailDefinitionComponent},
        { path: 'attaches/:attype/:idsrc', component: AttachListComponent},
        { path: 'projects', component: ProjectListComponent},
        { path: 'projects/details', component: ProjectDetailDefinitionComponent},
        { path: 'projects/details/:idpj', component: ProjectDetailDefinitionComponent},
        { path: 'projects/details/:idpj/sets', component: SetListComponent},
        { path: 'projects/details/:idpj/sets/details', component: SetDetailComponent},
        { path: 'projects/details/:idpj/sets/details/:idst', component: SetDetailComponent},
        { path: 'orders', component: OrderComponent},
        { path: 'shippings', component: ShippingComponent}
      ],
      canActivate: [CanActivateViaAuthGuard]
  }
  ,{ path: '**', component: FullLayout}
];

@NgModule({
  imports: [ RouterModule.forRoot(
    routes
    //,{ enableTracing: true }
  ) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
