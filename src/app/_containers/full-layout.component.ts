import { Component, OnInit, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

//import { AppBreadcrumbs} from '../_components'

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayout implements OnInit {

  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};
  
  public toggled(open: boolean): void {
    //console.log('Dropdown is now: ', open);
  }
  
  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  //constructor(private el: ElementRef) { }
  
    //wait for the component to render completely
  ngOnInit(): void {
    // var nativeElement: HTMLElement = this.el.nativeElement,
    // parentElement: HTMLElement = nativeElement.parentElement;
    // // move all children out of the element
    // while (nativeElement.firstChild) {
    //   parentElement.insertBefore(nativeElement.firstChild, nativeElement);
    // }
    // // remove the empty element(the host)
    // parentElement.removeChild(nativeElement);
  }

  private static breadcrumbs = {};

  constructor(private el: ElementRef,
    public translate: TranslateService,) {

    if (translate.currentLang == null)
    {
      translate.addLangs(["es", "en", "ru"]);
      translate.setDefaultLang('es');
  
      let browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/en|es/) ? browserLang : 'es');
    }
  }

  // ngOnInit(): void {
    
  // }
}
