import { Component, OnInit, ViewContainerRef, Input, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonModule, Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine } from '../_shared';
import { TranslationComponent } from '../translation'

import { Material, MaterialLanguage } from '../_models/';

import { AuthService } from '../auth';
import { MaterialService } from './material.service';

@Component({
    moduleId: module.id,
    selector: 'material-detail',
    templateUrl: 'material-detail.component.html'
})
export class MaterialDetailComponent {

    @Input() id: number;

    private validator: ValidatorEngine;
    private detailsForm: FormGroup;
    
    private isNew: boolean;
    public promiseEdit;

    public data: Material;
    public isDataReady: boolean;
    public isFormMetaReady: boolean;

    @ViewChild(TranslationComponent) translations: TranslationComponent
    
    constructor(
      private logger: NGXLogger,
      private materialService: MaterialService,
      private appServices: AppService,
      private authService: AuthService,
      private fb: FormBuilder,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private router: Router,
      private location: Location,
      private route: ActivatedRoute) {

        this.isDataReady = false;
        this.isFormMetaReady = false;

        //this.validator = new ValidatorEngine();
        //this.validator.Builded.on(this.formBuilded);
        this.isFormMetaReady = true
    }
  
    ngOnInit(): void {
        let sub = this.route.params.subscribe(params => {
            this.doInit(+params['idm']);
        });
    }

    private doInit(id: number)
    {
        this.id = (isNaN(id) ? null : id);

        this.isNew = (this.id == null);
        this.isDataReady = false;

        //this.getValidatorDefinition();

        if (!this.isNew)
            this.getData();
        else{
            this.data = new Material();
            this.data.MaterialLanguage = [ { IdLanguage : 1, IdMaterial:0, Description: '' }];
            this.isDataReady = true;
        }
            

    }
  
    // private getValidatorDefinition(): void {
    //     this.materialService.getValidator().then(d => {
    //         this.validator.BuildForm(d, this.translate);
    //         this.isFormMetaReady = true;
    //     });
    // }

    // private formBuilded = () => {
    //     this.detailsForm = this.validator.Form;
    //     this.isFormMetaReady = true;
    //     this.formLoaded();
    // }
      
    private getData() {
        this.materialService.getElement(this.id)
            .then(ar => {
                this.data = ar;
                //this.descTranslations = this.data.MaterialLanguage;
                this.isDataReady = true;
                //this.formLoaded();
            });
    }

    // private formLoaded()
    // {
    //    if (this.isFormMetaReady && this.isDataReady)
    //         (<FormGroup>this.detailsForm).patchValue(this.data, { onlySelf: true });
    // }

    public onCancel()
    {
        this.location.back();
    }
  
    public onSubmit() {
        this.toastr.clear();

        if (this.translations.isInvalid())
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("Error_TranslationsInvalid");
            return;    
        }
        // if (this.detailsForm.invalid)
        // {
        //     this.toastr.errorTranslate("_ReviewInvalidFileds");
        //     return;
        // }

        this.data.MaterialLanguage = this.translations.translations;

        if (this.isNew)
        {
            this.doCreate()
        }
        else
        {
            this.data.MaterialLanguage.forEach(m => {
                m.IdMaterial = this.id;
            });
            this.data.IdMaterial = this.id;
            this.doUpdate()
        }
      
    }

    private doUpdate()
    {
        this.promiseEdit = this.materialService.updateElement(this.data)
        .then(ar => {
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => 
        {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

    private doCreate()
    {
        this.promiseEdit = this.materialService.insertElement(this.data)
        .then(newId => {
            this.isNew = false;
            this.id = newId;
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

}

