import { Component, OnInit, OnDestroy, ViewChild, Input, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Material } from '../_models';

import { MaterialService } from './material.service'

@Component({
    moduleId: module.id,
    selector: 'agro-material-list',
    templateUrl: 'material-list.component.html'
})
export class MaterialListComponent implements OnDestroy {

    private tableTools: TableTools = new TableTools();
    private sub;

    constructor(
      private logger: NGXLogger,
      private materialService: MaterialService,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private dialogService: DialogService,
      private route: ActivatedRoute,
      private router: Router) {
        this.tableTools = new TableTools();

        this.sub = translate.onLangChange.subscribe((event) => {
            this.getData();
        })

    }

    ngOnInit() {
      this.getData();
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }
    
    rows: Material[] = [];
    isLoading = true;
  
    @ViewChild(DatatableComponent) table: DatatableComponent;

    getData()
    {
      this.isLoading = true;
      this.materialService.getList().then(data => { 
          this.rows = this.tableTools.init(data);
          this.isLoading = false;
          this.tableTools.applyFilter();
        })
      .catch(err => {
            this.toastr.errorTranslate("Error_Connection");
            this.isLoading = false;
          })
    }

    updateFilter(event, field) {
      this.rows = this.tableTools.updateFilter(event.target.value, field);
      this.table.offset = 0;
    }

    updateSelectionFilter(event, field) {
      let text = event.target.value;
      this.rows = this.tableTools.updateFilter(text, field);
      this.table.offset = 0;
    }
  
    onDelete(row)
    {
      let disposable = this.dialogService.addDialog(AppConfirmComponent, {
        title:'Delete_Title', 
        message:'Delete_ConfirmQuestion'})
        .subscribe((isConfirmed)=>{
            if(isConfirmed) {
              this.logger.trace(row.IdEntity);
              this.materialService.deleteElement(row.IdMaterial).then(res => {
                let index = this.rows.findIndex(d => d.IdMaterial === row.IdMaterial);
                this.rows.splice(index, 1);
                this.tableTools.remove("IdMaterial", row.IdMaterial);
                this.toastr.successTranslate("Delete_ConfirmMessage"); 
              })
              .catch(err => {
                this.toastr.errorTranslate(err);
              })
            }
        });
        
    }

    private getMaterialName(material: Material)
    {
      return material.MaterialLanguage.find(l => l.IdLanguage == 1).Description;
    }
}

