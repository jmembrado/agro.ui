import { Component, OnInit, ViewContainerRef, Input, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonModule, Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine, NgSelectTools } from '../_shared';
import { TranslationComponent } from '../translation'

import { Product, ProductLanguage } from '../_models/';

import { AuthService } from '../auth';
import { ProductService } from './product.service';
import { ProductMaterialComponent } from '../product-material';
import { PackingService } from '../packing';

@Component({
    moduleId: module.id,
    selector: 'product-detail',
    templateUrl: 'product-detail.component.html'
})
export class ProductDetailComponent {

    @Input() id: number;

    private validator: ValidatorEngine;
    private detailsForm: FormGroup;
    
    private isNew: boolean;
    public promiseEdit;

    public data: any;
    public isDataReady: boolean;
    public isFormMetaReady: boolean;
    public isRelatedDataReady: boolean;

    private packings;

    @ViewChild(TranslationComponent) translations: TranslationComponent
    @ViewChild(ProductMaterialComponent) productMaterials: ProductMaterialComponent
    
    constructor(
      private logger: NGXLogger,
      private productService: ProductService,
      private packingService: PackingService,
      private appServices: AppService,
      private authService: AuthService,
      private fb: FormBuilder,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private router: Router,
      private location: Location,
      private route: ActivatedRoute) {

        this.isDataReady = false;
        this.isFormMetaReady = false;
        this.isRelatedDataReady = false;

        this.validator = new ValidatorEngine();
        this.validator.Builded.on(this.formBuilded);
    }
  
    ngOnInit(): void {

        this.getValidatorDefinition();
        this.getPackings();

        let sub = this.route.params.subscribe(params => {
            this.doInit(+params['idpr']);
        });
    }

    private doInit(id: number)
    {
        this.id = (isNaN(id) ? null : id);

        this.isNew = (this.id == null);
        this.isDataReady = false;

        if (!this.isNew)
            this.getData();
        else{
            this.data = new Product();
            this.data.ProductLanguage = [];
            this.data.ProductMaterial = [];
            this.isDataReady = true;
            this.formLoaded();
        }
    }
  
    private getValidatorDefinition(): void {
        this.productService.getValidator().then(d => {
            this.validator.BuildForm(d, this.translate);
            this.isFormMetaReady = true;
        });
    }

    private formBuilded = () => {
        this.detailsForm = this.validator.Form;
        this.isFormMetaReady = true;
        this.formLoaded();
    }
      
    private getData() {
        this.productService.getElement(this.id)
            .then(ar => {
                this.data = ar;
                this.data.IdPacking = ar.IdPacking.toString();
                this.isDataReady = true;
                this.formLoaded();
        });
    }
    
    private getPackings()
    {
        this.packingService.getList()
            .then(ar => {
                this.packings = NgSelectTools.arrayToOptionMapper(ar, "IdPacking", "Description");
                this.isRelatedDataReady = true;
                this.formLoaded();
            });
    }

    private formLoaded()
    {
       if (this.isFormMetaReady && this.isDataReady && this.isRelatedDataReady)
       {
            (<FormGroup>this.detailsForm).patchValue(this.data, { onlySelf: true });
       }
    }

    public onCancel()
    {
        this.location.back();
    }
  
    public onSubmit() {
        this.toastr.clear();

        if (this.translations.isInvalid())
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("Error_TranslationsInvalid");
            return;    
        }
        if (this.productMaterials.isInvalid())
        {
            this.toastr.errorTranslate("Error_ProductMaterialInvalid");
            return;    
        }
        if (this.detailsForm.invalid)
        {
            this.toastr.errorTranslate("_ReviewInvalidFileds");
            return;
        }

        this.data = this.detailsForm.value;
        this.data.ProductLanguage = this.translations.translations;
        this.data.ProductMaterial = this.productMaterials.productMaterial;
        this.data.IdFormatLength = 1;
        this.data.IdFormatWeight = 1;
        this.data.IdPacking = parseInt(this.data.IdPacking);

        if (this.isNew)
        {
            this.data.IdProduct = 0;
            this.doCreate()
        }
        else
        {
            this.data.ProductMaterial.forEach(m => {
                m.IdProduct = this.id;
            });
            this.data.ProductLanguage.forEach(m => {
                m.IdProduct = this.id;
            });
            this.data.IdProduct = this.id;
            this.doUpdate()
        }
      
    }

    private doUpdate()
    {
        this.promiseEdit = this.productService.updateElement(this.data)
        .then(ar => {
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => 
        {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

    private doCreate()
    {
        this.promiseEdit = this.productService.insertElement(this.data)
        .then(newId => {
            this.isNew = false;
            this.id = newId;
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

}


