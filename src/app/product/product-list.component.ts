import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Product } from '../_models';

import { ProductService } from './product.service'
import { jsonpCallbackContext } from '@angular/common/http/src/module';

@Component({
    moduleId: module.id,
    selector: 'agro-product-list',
    templateUrl: 'product-list.component.html'
})
export class ProductListComponent {

  @Output() select = new EventEmitter();
  @Input() mode: string;
  @Input() excludeIds: number[];
  @Input() selectionType: string;

  private tableTools: TableTools = new TableTools();

  constructor(
    private logger: NGXLogger,
    private productService: ProductService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.tableTools = new TableTools();
      this.selectionType="single"
  }

  ngOnInit() {
    this.getData();
  }
  
  rows = [];
  isLoading = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  getData()
  {
    this.isLoading = true;
    this.productService.getList().then(data => { 
        if (this.excludeIds != null)
          this.tableTools.addException("IdProduct", this.excludeIds);
        this.rows = this.tableTools.init(data); ;
        this.isLoading = false;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }

  updateFilter(event, field) {
    this.rows = this.tableTools.updateFilter(event.target.value, field);
    this.table.offset = 0;
  }

  onDelete(row)
  {
    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.logger.trace(row.IdEntity);
            this.productService.deleteElement(row.IdProduct).then(res => {
              let index = this.rows.findIndex(d => d.IdProduct === row.IdProduct);
              this.rows.splice(index, 1);
              this.tableTools.remove("IdProduct", row.IdProduct);
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
            })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
  }

  onSelect(event)
  {
    this.select.emit(event.selected);
  }
}

