import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule }    from '@angular/http';
import { ReactiveFormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { FullLayout } from './_containers';

// Import components
import { AppSpinnerComponent, AppConfirmComponent, AppBreadcrumbs } from './_components';
const APP_COMPONENTS = [ AppSpinnerComponent, AppConfirmComponent, AppBreadcrumbs ]

// Import directives
import { AsideToggleDirective, NAV_DROPDOWN_DIRECTIVES, SidebarMinimizeDirective, SidebarOffCanvasCloseDirective
  , MobileSidebarToggleDirective, SidebarToggleDirective } from './_directives';
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  SidebarMinimizeDirective,
  MobileSidebarToggleDirective,
  SidebarToggleDirective,
  SidebarOffCanvasCloseDirective
  //,ModalDirective
]

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
//import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule  } from 'ngx-bootstrap/tooltip';
//import { ChartsModule } from 'ng2-charts/ng2-charts';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Angular2PromiseButtonModule } from 'angular2-promise-buttons';
import { SelectModule } from 'angular2-select';
import { NgUploaderModule } from 'ngx-uploader';
import { TreeModule } from 'angular-tree-component';
import { StarRatingModule } from 'angular-star-rating';
import { DateValueAccessorModule } from 'angular-date-value-accessor';


//Import AGRO Components
import { DashboardComponent } from './dashboard/dashboard.component'
import { AddressListComponent, AddressDetailDefinitionComponent } from './address'
import { AuthComponent } from './auth/auth.component'
import { AttachListComponent, AttachUploadComponent  } from './attach'
import { EntityListComponent, EntityDetailComponent, EntityDetailDefinitionComponent } from './entity'
import { FormatSelectorComponent } from './format'
import { MaterialListComponent, MaterialDetailComponent } from './material'
import { PackingListComponent, PackingDetailComponent } from './packing'
import { ProductListComponent, ProductDetailComponent } from './product'
import { ProductMaterialComponent } from './product-material'
import { ProjectListComponent, ProjectDetailDefinitionComponent } from './project'
import { SetListComponent, SetDetailComponent, SetProductListComponent } from './set'
import { TemplateListComponent, TemplateTreeEditorComponent, TemplateTreeComponent, TemplateDetailDefinitionComponent } from './template'
import { TranslationComponent } from './translation'

import { OrderComponent } from './order/order.component'
import { ShippingComponent } from './shipping/shipping.component'


const AGRO_COMPONENTS = [ 
  AddressListComponent, AddressDetailDefinitionComponent, 
  AttachListComponent, AttachUploadComponent,
  AuthComponent, 
  DashboardComponent,
  EntityListComponent, EntityDetailComponent, EntityDetailDefinitionComponent,
  FormatSelectorComponent,
  MaterialListComponent, MaterialDetailComponent,
  PackingListComponent, PackingDetailComponent,
  ProductListComponent, ProductDetailComponent, ProductMaterialComponent,
  ProjectListComponent, ProjectDetailDefinitionComponent,
  SetListComponent, SetDetailComponent, SetProductListComponent,
  TemplateListComponent, TemplateTreeEditorComponent, TemplateTreeComponent, TemplateDetailDefinitionComponent,  
  TranslationComponent,

  OrderComponent, ShippingComponent
]

import { AppService } from './app.service';
import { AppToastrService } from './_shared'

import { AddressService } from './address'
import { AttachService } from './attach'
import { AuthService, CanActivateViaAuthGuard } from './auth'
import { CountryService } from './country'
import { EntityService } from './entity'
import { FormatService } from './format'
import { LanguageService, TranslationService } from './translation'
import { MaterialService } from './material'
import { PackingService } from './packing'
import { ProductService } from './product'
import { ProductMaterialService } from './product-material'
import { ProjectService } from './project'
import { SetService, SetProductService } from './set'
import { TemplateService, TemplateTreeService } from './template'

const AGRO_SERVICES = [ 
  AppService, AppToastrService,LanguageService, TranslationService,
  AuthService, CanActivateViaAuthGuard, 
  AttachService,
  EntityService, AddressService, CountryService,
  MaterialService, PackingService, FormatService,
  ProductService, ProductMaterialService,
  ProjectService,
  SetService, SetProductService, 
  TemplateService, TemplateTreeService
]


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    LoggerModule.forRoot({serverLoggingUrl: '/api/logs', 
      level: NgxLoggerLevel.TRACE, 
      serverLogLevel: NgxLoggerLevel.OFF}),
    Ng2DeviceDetectorModule.forRoot(),
    TabsModule.forRoot(),
    ToastrModule.forRoot({ 
      timeOut: 0
    }),
    TooltipModule.forRoot(),
    NgxDatatableModule,
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    }),
    HttpModule,
    ReactiveFormsModule,
    CommonModule,
    Angular2PromiseButtonModule.forRoot({
      spinnerTpl: '<div class="spinner-mini"><div class="double-bounce1 bounce-neutral"></div><div class="double-bounce2 bounce-neutral"></div></div>',
      disableBtn: true,
      btnLoadingClass: 'is-loading',
      handleCurrentBtnOnly: false,
    }),
    BootstrapModalModule, 
    SelectModule,
    NgUploaderModule,
    TreeModule,
    StarRatingModule.forRoot(),
    DateValueAccessorModule
  ],
  entryComponents: [
    AppConfirmComponent
  ],
  declarations: [
    AppComponent,
    FullLayout,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES,
    ...AGRO_COMPONENTS,
  ],
  providers: [{
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    ...AGRO_SERVICES
    
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }


