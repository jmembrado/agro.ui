import { Component, OnInit, OnDestroy, ViewContainerRef, Input  } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonModule, Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine, NgSelectTools } from '../_shared';

import { Address, Country } from '../_models/';

import { AuthService } from '../auth';
import { AddressService } from './address.service';
import { CountryService } from '../country';

@Component({
    moduleId: module.id,
    selector: 'address-detail-definition',
    templateUrl: 'address-detail-definition.component.html'
})
export class AddressDetailDefinitionComponent implements OnDestroy {

    private sub;

    @Input() id: number;
    @Input() idEntity: number;
    @Input() data: any;

    private countries;
    private addressTypes;

    private validator: ValidatorEngine;
    private detailsForm: FormGroup;
    private textboxFields;
    
    private isNew: boolean;
    public promiseEdit;

    public isDataReady: boolean;
    public isRelatedDataReady: boolean;
    public isFormMetaReady: boolean;
    
    constructor(
      private logger: NGXLogger,
      private addressService: AddressService,
      private countryService: CountryService,
      private appServices: AppService,
      private authService: AuthService,
      private fb: FormBuilder,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private router: Router,
      private location: Location,
      private route: ActivatedRoute) {
        
        this.addressTypes = this.addressService.getAddressTypes(this.translate);
        
        this.sub = translate.onLangChange.subscribe((event) => {
            this.addressService.translateAddressTypes(this.translate);
        })
        
        this.textboxFields = ["Line1", "Line2", "PostCode"];

        this.isDataReady = false;
        this.isFormMetaReady = false;
        this.isRelatedDataReady = false;

        this.validator = new ValidatorEngine();
        this.validator.Builded.on(this.formBuilded);
    }
  
    ngOnInit(): void {
        this.getCountries();

        this.route.params.subscribe(params => {
            this.id = params['ida']; 

            this.route.parent.params.subscribe(params => {
                this.idEntity = params['id']; 
    
                this.doInit();
            });
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
      }

    private doInit()
    {
        this.isNew = (this.id == null);
        this.isDataReady = (this.isNew);

        this.getValidatorDefinition();

        if (!this.isNew)
            this.getData();
        else
            this.data = new Address();
    }
  
    private getValidatorDefinition(): void {
        this.addressService.getValidator().then(d => {
            this.validator.BuildForm(d, this.translate);
            this.isFormMetaReady = true;
        });
    }

    private getCountries(){
        this.countryService.getListToOptionMapper(1,1).then(d => {
            this.countries = d;
            this.isRelatedDataReady = true;
            this.formLoaded();
        });
    }

    private formBuilded = () => {
        this.detailsForm = this.validator.Form;
        this.isFormMetaReady = true;
        this.formLoaded();
    }
      
    private getData() {
        this.addressService.getElement(this.id)
            .then(ar => {
                this.data = ar;
                this.isDataReady = true;
                this.formLoaded();
            });
    }

    private formLoaded()
    {
        if (this.isFormMetaReady && this.isDataReady && this.isRelatedDataReady)
        {
            if (this.data.IdCountry != null)
                this.data.IdCountry = this.data.IdCountry.toString();
            (<FormGroup>this.detailsForm).patchValue(this.data, { onlySelf: true });
            //this.detailsForm.controls["Type"].setValue([this.data.Type]);
        }
            
    }

    public onCancel()
    {
        this.location.back();
    }
  
    public onSubmit() {
    
        this.toastr.clear();
        if (this.detailsForm.invalid)
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("_ReviewInvalidFileds");
            return;
        }
  
        this.data = this.detailsForm.value;
        //this.data.Type = this.detailsForm.controls["Type"].value;
        this.data.IdCountry = parseInt(this.data.IdCountry);

        this.data.IdEntity = this.idEntity;
        
        if (this.isNew)
        {
            this.doCreate()
        }
        else
        {
            this.data.IdAddress = this.id;
            this.doUpdate()
        }
      
    }

    private doUpdate()
    {
        this.promiseEdit = this.addressService.updateElement(this.data)
        .then(ar => {
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => 
        {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

    private doCreate()
    {
        this.promiseEdit = this.addressService.insertElement(this.data)
        .then(newId => {
            this.isNew = false;
            this.id = newId;
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

}

