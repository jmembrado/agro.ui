import { Component, OnInit, OnDestroy, ViewChild, Input, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Address } from '../_models';

import { AddressService } from './address.service'

@Component({
    moduleId: module.id,
    selector: 'agro-address-list',
    templateUrl: 'address-list.component.html'
})
export class AddressListComponent {

  private sub;

  @Input() idEntity: number;

  private tableTools: TableTools = new TableTools();

  private addressTypes;

  constructor(
    private logger: NGXLogger,
    private addressService: AddressService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.tableTools = new TableTools();

      this.addressTypes = this.addressService.getAddressTypes(this.translate);
      
      this.sub = translate.onLangChange.subscribe((event) => {
        this.addressService.translateAddressTypes(this.translate);
      })

  }

  ngOnInit() {
    if (this.idEntity == undefined) {
      this.route.parent.params.subscribe(params => { 
          this.idEntity = +params['id'];
          this.doInit();
      });
    }
    else {
        this.doInit();
    }
      
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private doInit()
  {
    this.getData();
  }
  
  rows = [];
  isLoading = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  private getData()
  {
    this.isLoading = true;
    this.addressService.getList(this.idEntity).then(data => { 
        this.rows = this.tableTools.init(data);
        this.isLoading = false;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }

  updateFilter(event, field) {
    this.rows = this.tableTools.updateFilter(event.target.value, field);
    this.table.offset = 0;
  }

  updateSelectionFilter(event, field) {
    let text = event.target.value;
    this.rows = this.tableTools.updateFilter(text, field);
    this.table.offset = 0;
  }

  onDelete(row)
  {
    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.logger.trace(row.IdEntity);
            this.addressService.deleteElement(row.IdAddress).then(res => {
              let index = this.rows.findIndex(d => d.IdAddress === row.IdAddress);
              this.rows.splice(index, 1);
              this.tableTools.remove("IdAddress", row.IdAddress);
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
            })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
      
  }

  private getCountryName(address: Address)
  {
    return address.IdCountryNavigation.CountryLanguage.find(c => c.IdLanguage == 1).Name;
  }
}

