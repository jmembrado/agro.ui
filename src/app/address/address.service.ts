import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NGXLogger } from 'ngx-logger';

import { Address } from '../_models';
import { CountryService } from '../country'

import { ApiResponse, ValidatorField } from '../_shared';
import { AuthService } from '../auth'

@Injectable()
export class AddressService {

    private serviceUrl = '/api/Addresses';
    
    constructor(private logger: NGXLogger,
        private http: Http,
        private authService: AuthService,
        private countryService: CountryService) { }

    private handleError(error: any): Promise<any> {
        if (error.status)
        {
            switch (error.status)
            {
                case 401:
                    this.authService.found401();
                    return Promise.resolve("Error_401"); 
                case 400:
                    if (error._body != null && error._body.length > 0)
                    {
                        let respErr = JSON.parse(error._body) as ApiResponse;
                        return Promise.reject(respErr.ResultCode);
                    }
                    else
                    {
                        this.logger.error(error);
                        return error;
                    }                    
            }

        }

        this.logger.error(error);
            
        return Promise.reject(error.message || error);
    }

    public getValidator(): Promise<ValidatorField[]> {
        const url = `api/Validator/Address`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => {
                return response.json() as ValidatorField[];
            })
            .catch(this.handleError);
    }

    public getList(idEntity:number): Promise<Address[]> {
        let url = `${this.serviceUrl}/${idEntity}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // private addCountries(addresses: Address[])
    // {
    //     this.countryService.getList()
    //         .then(allCountries => 
    //         {
    //             addresses.forEach(ad => {
    //                 if (ad.IdCountry != null)
    //                 {
    //                     let c = allCountries.find(c => c.IdCountry == ad.IdCountry);
    //                     ad.CountryTranlations = [];
    //                     c.CountryLanguage.map( l => {
    //                         return { l.}
    //                     })
    //                 }
    //             })
    //         });
    // }

    public getElement(id: number): Promise<Address> {
        const url = `${this.serviceUrl}/details/${id}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public updateElement(element: Address): Promise<any> {
        const url = `${this.serviceUrl}/${element.IdAddress}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.put(url, JSON.stringify(element), { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public insertElement(element: Address): Promise<number> {
        const url = `${this.serviceUrl}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.post(url, JSON.stringify(element), { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public deleteElement(id: number): Promise<any> {
        const url = `${this.serviceUrl}/${id}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.delete(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private types = [ {value:"Main", label:null },  
    { value:"Invoice", label:null}, 
    { value:"Shipping", label:null}, 
    { value:"Other", label:null} ];
    public getAddressTypes(traslateService)
    {
        this.translateAddressTypes(traslateService)
        return this.types;
    }

    public translateAddressTypes(traslateService)
    {
        for (let entry of this.types) {
            traslateService.get('Address_Type_' + entry.value).subscribe((res: string) => {
                entry.label = res;
            });
        }
    }

}