import { Component, OnInit, Input, Output, EventEmitter, ViewChild  } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine, NgSelectTools } from '../_shared';
import { TranslationComponent } from '../translation'

import { Set, SetLanguage } from '../_models';

import { AuthService } from '../auth';
import { SetService } from './set.service';

@Component({
    moduleId: module.id,
    selector: 'agro-set-detail',
    templateUrl: 'set-detail.component.html'
})
export class SetDetailComponent {

    // @Input() mode: string;
    
    // @Input('idSet')
    // set idSet(value: number) {
    //     this.id = value;
    //     this.doInit();
    // }
    private idProject: number;
    // @Output() onSave = new EventEmitter();

    private id: number;
    private validator: ValidatorEngine;
    private detailsForm: FormGroup;
    
    private isNew: boolean;
    public promiseEdit;

    public data: any;
    public isDataReady: boolean;
    public isFormMetaReady: boolean;
    public isRelatedDataReady: boolean;

    private transColumns;

    private ratingValue;

    @ViewChild(TranslationComponent) translations: TranslationComponent
    
    constructor(
      private logger: NGXLogger,
      private setService: SetService,
      private appServices: AppService,
      private authService: AuthService,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private router: Router,
      private location: Location,
      private route: ActivatedRoute) {

        this.transColumns = [
            {label: 'Set_Name', prop: 'Name', isMultiline: false, isRequired: true, maxLength: 200}]

        this.isDataReady = false;
        this.isFormMetaReady = false;
        this.isRelatedDataReady = true;

        this.validator = new ValidatorEngine();
        this.validator.Builded.on(this.formBuilded);
    }
  
    ngOnInit(): void {

        this.getValidatorDefinition();

        this.route.params.subscribe(params => {
            this.idProject = +params['idpj'];
            this.doInit(+params['idst']);
          });
    }

    private doInit(id:number)
    {
        this.id = (isNaN(id) ? null : id);
        
        this.isNew = (this.id == null);
        this.isDataReady = false;

        if (!this.isNew)
            this.getData();
        else{
            this.data = new Set();
            this.data.SetLanguage = [ { IdLanguage : 1, IdSet:0, Description: '' }];
            this.data.isOpen = true;
            this.data.Priority = 1;
            this.isDataReady = true;
            this.formLoaded();
        }
    }
  
    private getValidatorDefinition(): void {
        this.setService.getValidator().then(d => {
            this.validator.BuildForm(d, this.translate);
            this.isFormMetaReady = true;
        });
    }

    private formBuilded = () => {
        this.detailsForm = this.validator.Form;
        this.isFormMetaReady = true;
        this.formLoaded();
    }
      
    private getData() {
        this.setService.getElement(this.id)
            .then(ar => {
                this.data = ar;
                if (ar.Date != null)
                    this.data.Date = new Date(ar.Date);
                if (ar.EndDate != null)
                    this.data.EndDate = new Date(ar.EndDate);
                this.isDataReady = true;
                this.formLoaded();
        });
    }

    private formLoaded()
    {
       if (this.isFormMetaReady && this.isDataReady && this.isRelatedDataReady)
       {
            (<FormGroup>this.detailsForm).patchValue(this.data, { onlySelf: true });
            if (this.data != null && this.data.Priority != null)
                this.ratingValue = this.data.Priority;
       }
    }

    public onCancel()
    {
        this.location.back();
    }
  
    public onSubmit() {
        this.toastr.clear();

        if (this.translations.isInvalid())
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("Error_TranslationsInvalid");
            return;    
        }
        if (this.detailsForm.invalid)
        {
            this.toastr.errorTranslate("_ReviewInvalidFileds");
            return;
        }

        this.data = this.detailsForm.value;
        this.data.IdProyect = this.idProject;
        this.data.Priority = this.ratingValue;
        this.data.SetLanguage = this.translations.translations;

        if (this.isNew)
        {
            this.data.IdSet = 0;
            this.doCreate()
        }
        else
        {
            this.data.SetLanguage.forEach(m => {
                m.IdSet = this.id;
            });
            this.data.IdSet = this.id;
            this.doUpdate()
        }

    }

    private doUpdate()
    {
        this.promiseEdit = this.setService.updateElement(this.data)
        .then(ar => {
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => 
        {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

    private doCreate()
    {
        this.promiseEdit = this.setService.insertElement(this.data)
        .then(newId => {
            this.isNew = false;
            this.id = newId;
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

}


