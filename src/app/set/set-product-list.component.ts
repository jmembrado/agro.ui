import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';


import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { StarRatingComponent } from 'angular-star-rating';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { SetProductService } from './set-product.service';
import { fail } from 'assert';

@Component({
    moduleId: module.id,
    selector: 'agro-set-product-list',
    templateUrl: 'set-product-list.component.html'
})
export class SetProductListComponent {

  @Input() idSet: number;

  isAdding: boolean;

  private tableTools: TableTools = new TableTools();

  private boolFilter;
  private sub;

  private editingRows;

  isNaN: Function = Number.isNaN;
  
  rows = [];
  isLoading = true;
  ratingValue = 0;

  @ViewChild(DatatableComponent) table: DatatableComponent
  @ViewChild(StarRatingComponent) starRating: StarRatingComponent

  constructor(
    private logger: NGXLogger,
    private setProductService: SetProductService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.tableTools = new TableTools();
      
      this.sub = translate.onLangChange.subscribe((event) => {
        this.getBoolFilter();
      })

      for (let i = 0; i<5; i++)
      {
        this.rows.push({IdProduct:0});
      }

      this.isAdding = false;
      this.editingRows = {};
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private getData()
  {
    this.isLoading = true;

    this.getBoolFilter();

    this.setProductService.getList(this.idSet).then(data => { 
        this.rows = this.tableTools.init(data);
        this.isLoading = false;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }

  private getBoolFilter()
  {
      this.boolFilter = [ //{value:null, label:'' },  
      { value:true, label:null}, 
      { value:false, label:null}];
  
      for (let entry of this.boolFilter) {
        //if (entry.value != null)
        {
          this.translate.get('_' + entry.value).subscribe((res: string) => {
            entry.label = res;
          });
        }
      }
  }

  updateTextFilter(event, field) {
    this.rows = this.tableTools.updateFilter(event.target.value, field);
    this.table.offset = 0;
  }

  clearRatingFilter()
  {
    this.starRating.rating = 0;
    this.ratingValue = 0;
  }

  updateRatingFilter(event) {
    this.ratingValue = event.rating;
    this.rows = this.tableTools.updateFilter(event.rating, 'Priority');
    this.table.offset = 0;
  }

  updateSelectionFilter(event, field) {
    let text = event.target.value;
    this.rows = this.tableTools.updateFilter(text, field);
    this.table.offset = 0;
  }

  onDelete(row, rowIndex)
  {
    if (row.isNew)
    {
      this.rows.splice(rowIndex, 1);
      return;
    }

    this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.logger.trace(row.IdEntity);
            this.setProductService.deleteElement(row.IdSet, row.IdProduct).then(res => {
              let index = this.rows.findIndex(d => d.IdProduct === row.IdProduct);
              this.rows.splice(index, 1);
              this.tableTools.remove("IdProduct", row.IdProduct);
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
             })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
  }

  onBeginEdit(row, rowIndex)
  {
    this.editingRows[rowIndex] = row;
  }

  onSaveEditingRow(rowIndex)
  {
    let row = this.editingRows[rowIndex];
    if (row.isNew)
    {
      this.setProductService.insertElement(this.row2ProductSetTransform(row)).then(el => 
      {
        delete row["isNew"];
        delete this.editingRows[rowIndex];

        this.rows = [...this.rows];
        this.toastr.successTranslate("_DataSuccessfullySaved");
      })
    }
    else
    {
      this.setProductService.updateElement(this.row2ProductSetTransform(row)).then(el => 
      {
        delete this.editingRows[rowIndex];
        
        this.rows = [...this.rows];
        this.toastr.successTranslate("_DataSuccessfullySaved");
      })
    }
  }

  row2ProductSetTransform(row)
  {
    let setProduct = { IdProduct : row.IdProduct, IdSet : row.IdSet };
    if (row["Units"] != null)
      setProduct["Units"] = parseInt(row["Units"]);
    if (row["Price"] != null)
      setProduct["Price"] = parseFloat(row["Price"]);
    if (row["Date"] != null)
      setProduct["Date"] = row["Date"];
    return setProduct;
  }

  onCancelEdit(rowIndex)
  {
    let row = this.editingRows[rowIndex];
    if (row.isNew)
      this.rows.splice(rowIndex, 1);

    delete this.editingRows[rowIndex];
  }

  updateEditingValue(event, cell, rowIndex) 
  {
    this.editingRows[rowIndex][cell] = event.target.value;
  }

  formatCurrency(value)
  {
    if (value != null && typeof value != "string" && !isNaN(value))
    {
      let decimalPipe = new DecimalPipe("es-ES");
      return decimalPipe.transform(value, "1.2-2") + " €";
    }
    else 
    {
      return ""
    }
  }

  private productSelected;
  public onProductAdd()
  {
    if (this.productSelected == null || this.productSelected.length == 0)
    {
      this.toastr.errorTranslate("Error_TemplateMustSelectOneProduct");
      return;
    }

    this.isAdding = false;

    this.productSelected.forEach(element => {
      let row = { isNew: true, Reference : this.rows.length + 1, IdProduct: element.IdProduct, 
        IdSet : this.idSet, IdProductNavigation: {...element} } ;
      this.rows.push( row );

      let rowIndex = this.rows.length - 1;
      this.editingRows[rowIndex] = row;
    });
    
    
    //this.rows = [...this.rows];

    // this.isLoading = true;

    // this.setProductService.insertElement({ IdProduct : this.productSelected.IdProduct, IdSet : row.IdSet }).then(p=>
    // {
    //   this.setProductService.getList(this.idSet).then(data => { 
    //     this.rows = this.tableTools.init(data);
    //     this.isLoading = false;
    //     this.isAdding = false; 

    //     this.productSelected = null;
        
    //     this.toastr.successTranslate("_DataSuccessfullySaved");
    //   })
    //   .catch(err => {
    //         this.toastr.errorTranslate("Error_Connection");
    //         this.isLoading = false;
    //       })
    // })
  }

  public onProductAddBegin()
  {
    this.isAdding = true; 
    this.productSelected = null;
  }

  public onProductAddCancel()
  {
    this.isAdding = false; 
    this.productSelected = null;
  }

  public onProductSelected(product)
  {
    this.productSelected = product;
  }

  private getProductIds()
  {
    let ids : number[] = null;
    if (this.rows != null && this.rows.length > 0)
    {
      ids = [];
      this.rows.forEach(element => {
        ids.push(element.IdProduct);  
      });
    }
    return ids;
  }

  private isEditing(rowIndex): boolean
  {
    return this.editingRows.hasOwnProperty(rowIndex.toString());
  }
}

