import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Set } from '../_models';

import { SetService } from './set.service'
import { SetProductService  } from './set-product.service'

@Component({
    moduleId: module.id,
    selector: 'agro-set-list',
    templateUrl: 'set-list.component.html'
})
export class SetListComponent {

  public idProject: number;

  private tableTools: TableTools = new TableTools();

  rows = [];
  isLoading = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private logger: NGXLogger,
    private setService: SetService,
    private setProductService: SetProductService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.tableTools = new TableTools();
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.idProject = +params['idpj'];
      this.getData();
    });
  }
  
  getData()
  {
    this.isLoading = true;
    this.setService.getList(this.idProject).then(data => { 
        this.rows = this.tableTools.init(data); 
        this.isLoading = false;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }

  updateFilter(event, field) {
    this.rows = this.tableTools.updateFilter(event.target.value, field);
    this.table.offset = 0;
  }

  onDelete(row)
  {
    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.logger.trace(row.IdEntity);
            this.setService.deleteElement(row.IdSet).then(res => {
              let index = this.rows.findIndex(d => d.IdSet === row.IdSet);
              this.rows.splice(index, 1);
              this.tableTools.remove("IdSet", row.IdSet);
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
            })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

}

