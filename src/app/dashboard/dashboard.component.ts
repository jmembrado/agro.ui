import { Component } from '@angular/core';

@Component({
  template: `<div class="animated fadeIn">
  <div class="card">
    <div class="card-header">Bienvenido a la v0.1.0.0 Beta</div>
    <div class="card-body">
      Esta es una versión de pruebas donde se pueden realizar las siguientes acciones:
      <ul>
        <li>Clientes: Creación, Edición, Inserción, Eliminación
          <ul>
            <li>Direcciones:  Creación, Edición, Inserción, Eliminación</li>
          </ul>
        </li>
        <li>Proveedores: Creación, Edición, Inserción, Eliminación
          <ul>
            <li>Direcciones:  Creación, Edición, Inserción, Eliminación</li>
          </ul>
        </li>
        <li>Materiales: Creación, Edición, Inserción, Eliminación</li>
        <li>Empaquetados: Creación, Edición, Inserción, Eliminación</li>
        <li>Productos: Creación, Edición, Inserción, Eliminación
          <ul>
            <li>Adjuntar archivos:  Cargar, Descargar, Eliminar</li>
            <li>Asignar Materiales</li>
          </ul>
        </li>
        <li>Plantillas: Creación, Edición, Inserción, Eliminación
        <ul>
          <li>Vista de Árbol: Crear sub-plantillas</li>
          <li>Asingar Productos</li>
          <li>Quitar Productos</li>
        </ul>
      </li>
        <li>Proyectos: Creación, Edición, Inserción, Eliminación
          <ul>
            <li>Crear Sets:  Cargar, Descargar, Eliminar</li>
            <li>Asignar Productos</li>
          </ul>
        </li>
        <li>Traducción autómatica muti-idioma para las Descripciónes de:
          <ul>
            <li>Países</li>
            <li>Materiales</li>
            <li>Empaquetados</li>
            <li>Proyectos</li>
            <li>Productos</li>
            <li>Regiones</li>
            <li>Sets</li>
            <li>Plantillas</li>
          </ul>
        </li>
      </ul>
    </div>
  </div>`
  //,templateUrl: 'dashboard.component.html'
})
export class DashboardComponent  {

  
}
