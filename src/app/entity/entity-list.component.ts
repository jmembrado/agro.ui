import { Component, OnInit, ViewChild, Input, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Entity } from '../_models';

import { EntityService } from './entity.service'

@Component({
    moduleId: module.id,
    selector: 'agro-entity-list',
    templateUrl: 'entity-list.component.html'
})
export class EntityListComponent {

    @Input() type: string = 'cust';

    private tableTools: TableTools = new TableTools();

    private sub: any;

    constructor(
      private logger: NGXLogger,
      private entityService: EntityService,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private dialogService: DialogService,
      private route: ActivatedRoute,
      private router: Router) {
    }

    ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
        this.type = params['type']; 
        this.getData();
      });
        
    }
    
    rows = [];
    isLoading = true;
  
    @ViewChild(DatatableComponent) table: DatatableComponent;

    getData()
    {
      this.isLoading = true;
      this.entityService.getList(this.type).then(data => { 
          this.rows = this.tableTools.init(data);
          this.isLoading = false;
        })
      .catch(err => {
            this.toastr.errorTranslate("Error_Connection");
            this.isLoading = false;
          })
    }

    updateFilter(event, field) {
      this.rows = this.tableTools.updateFilter(event.target.value, field);
      this.table.offset = 0;
    }
  
    onDelete(row)
    {
      let disposable = this.dialogService.addDialog(AppConfirmComponent, {
        title:'Delete_Title', 
        message:'Delete_ConfirmQuestion'})
        .subscribe((isConfirmed)=>{
            if(isConfirmed) {
              this.logger.trace(row.IdEntity);
              this.entityService.deleteElement(this.type, row.IdEntity).then(res => {
                let index = this.rows.findIndex(d => d.IdEntity === row.IdEntity);
                this.rows.splice(index, 1);
                this.tableTools.remove("IdEntity", row.IdEntity);
                this.toastr.successTranslate("Delete_ConfirmMessage"); 
              })
              .catch(err => {
                this.toastr.errorTranslate(err);
              })
            }
        });
        
    }

}

