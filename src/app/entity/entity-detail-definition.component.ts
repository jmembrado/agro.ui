import { Component, OnInit, ViewContainerRef, Input  } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonModule, Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine } from '../_shared';

import { Entity } from '../_models/';

import { AuthService } from '../auth';
import { EntityService } from './entity.service';
import { CountryService } from '../country';

@Component({
    moduleId: module.id,
    selector: 'entity-detail-definition',
    templateUrl: 'entity-detail-definition.component.html'
})
export class EntityDetailDefinitionComponent {

    @Input() type: string;
    @Input() id: number;
    @Input() data: any;

    private countries = [];

    private validator: ValidatorEngine;
    private detailsForm: FormGroup;
    private textboxFields;
    
    private isNew: boolean;
    public promiseEdit;

    public isDataReady: boolean;
    public isFormMetaReady: boolean;
    
    constructor(
      private logger: NGXLogger,
      private entityService: EntityService,
      private countryService: CountryService,
      private appServices: AppService,
      private authService: AuthService,
      private fb: FormBuilder,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private router: Router,
      private location: Location,
      private route: ActivatedRoute) {

        this.isDataReady = false;
        this.isFormMetaReady = false;        

        this.textboxFields = ["Doc", "Name", "SocialName", "Phone1", "Phone2"];

        this.validator = new ValidatorEngine();
        this.validator.Builded.on(this.formBuilded);
       
    }
  
    ngOnInit(): void {
        //this.countries = [ {value : 724, label:"Spain"}, {value : 643, label:"Russia"}, {value : 840, label:"USA"}];
        this.getCountries();

        let sub = this.route.params.subscribe(params => {
            if (params['type'] != null)
            {
                this.doInit(params['type'], null);
            }

        });

        let subp = this.route.parent.params.subscribe(params => {
            if (params['type'] != null)
            {
                this.doInit(params['type'], +params['id']);
            }
        });
    }

    private doInit(type: string, id: number)
    {
        this.type = type;
        this.id = id;

        this.isNew = (this.id == null);
        this.isDataReady = (this.isNew);

        this.getValidatorDefinition();

        if (!this.isNew)
            this.getData();
        else
            this.data = {};
    }
  
    private getValidatorDefinition(): void {
        this.entityService.getValidator().then(d => {
            this.validator.BuildForm(d, this.translate);
            this.isFormMetaReady = true;
        });
    }

    private getCountries(){
        this.countryService.getListToOptionMapper(1,1).then(d => {
            this.countries = d;
        });
    }

    private formBuilded = () => {
        this.detailsForm = this.validator.Form;
        this.isFormMetaReady = true;
        this.formLoaded();
    }
      
    private getData() {
        this.entityService.getElement(this.type, this.id)
            .then(ar => {
                this.data = ar;
                this.isDataReady = true;
                this.formLoaded();
            });
    }

    private formLoaded()
    {
       if (this.isFormMetaReady && this.isDataReady)
       {
            if (this.data.IdCountry != null)
                this.data.IdCountry = this.data.IdCountry.toString();
            (<FormGroup>this.detailsForm).patchValue(this.data, { onlySelf: true });
       }
            
    }


    public onCancel()
    {
        this.location.back();
    }
  
    public onSubmit() {
        this.toastr.clear();
        if (this.detailsForm.invalid)
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("_ReviewInvalidFileds");
            return;
        }
  
        this.data = this.detailsForm.value;
        this.data.IdCountry = parseInt(this.data.IdCountry);

        if (this.isNew)
        {
            this.doCreate()
        }
        else
        {
            this.data.IdEntity = this.id;
            this.doUpdate()
        }
      
    }

    private doUpdate()
    {
        this.promiseEdit = this.entityService.updateElement(this.type, this.data)
        .then(ar => {
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => 
        {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

    private doCreate()
    {
        this.promiseEdit = this.entityService.insertElement(this.type, this.data)
        .then(newId => {
            this.isNew = false;
            this.id = newId;
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

}

