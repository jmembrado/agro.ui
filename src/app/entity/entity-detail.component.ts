import { Component, ViewChild, OnInit, Input  } from '@angular/core';
import { CommonModule, Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { TabsetComponent } from 'ngx-bootstrap';

import { ApiResponse, AppToastrService } from '../_shared';

import { AddressListComponent } from '../address'

import { Entity } from '../_models/';

import { AuthService } from '../auth';
import { EntityService } from './entity.service';

@Component({
    moduleId: module.id,
    selector: 'entity-detail',
    templateUrl: 'entity-detail.component.html'
})
export class EntityDetailComponent {
    @Input() type: string = 'customer';
    @Input() id: number;
    public name: string;
    private sub: any;
    
    private isNew: boolean;
    public data: Entity;
    public isDataReady: boolean;
    
    constructor(
      private logger: NGXLogger,
      private entityService: EntityService,
      private authService: AuthService,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private route: ActivatedRoute) {

        this.isNew = true;
        this.isDataReady = false;

        if (this.isNew)
        {
            this.translate.get("_New").subscribe((res: string) => {
                this.name = res;
             });
        }
    }
  
    ngOnInit(): void {

        this.sub = this.route.params.subscribe(params => {
            this.type = params['type']; 
            this.id = params['id']; 

            this.isNew = (this.id == null);
            this.isDataReady = (this.isNew);

            if (!this.isNew)
                this.getData();
            else
                this.data = new Entity();
        });
    }
  
    private getData() {
        this.entityService.getElement(this.type, this.id)
            .then(ar => {
                this.data = ar;
                this.name  = this.data.Name;
                this.isDataReady = true;
            });
    }

}

