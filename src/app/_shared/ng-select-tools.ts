export class NgSelectTools {
    

    public static arrayToOptionMapper(array: any[], idField:string, labelField: string)
    {
        let res = [];
        array.forEach(element => {
            let idValue = idField.split('.').reduce((a, b) => a[b], element);
            let labelValue = labelField.split('.').reduce((a, b) => a[b], element);
            res.push({value: idValue.toString(), label: labelValue});
        });
        return res;
    }
}