import { Injectable } from "@angular/core";
import { ToastrService, IndividualConfig, ActiveToast } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

export class toastrMessage {
    message: string;
    caption?: string;
}

@Injectable()
export class AppToastrService {

    constructor(private toastr: ToastrService, 
        private translate: TranslateService,) { 
            
    }

    public clear (toastId?:number)
    {
        this.toastr.clear(toastId)
    }

    public warningTranslate (text:string, caption?:string, override?:IndividualConfig):ActiveToast
    { 
        return this.show("warning", text, caption, override);
    }

    public successTranslate (text:string, caption?:string, override?:IndividualConfig):ActiveToast
    {
        return this.show("success", text, caption, override);
    }

    public infoTranslate (text:string, caption?:string, override?:IndividualConfig):ActiveToast
    {
        return this.show("info", text, caption, override);
    }

    public errorTranslate (text:string, caption?:string, override?:IndividualConfig):ActiveToast
    {
        return this.show("error", text, caption, override);
    }

    private show (type: string, text: string, caption?:string, override?:IndividualConfig): ActiveToast
    {
        if (caption == null && type.length > 0)
            caption = '_' + type;
        let msg = this.translateMesssage(text, caption);
        let gOptions = this.setDefaults(override);
        return this.toastr[type](msg.message, msg.caption, gOptions);
    }


    private translateMesssage(text:string, caption?:string): toastrMessage
    {
        let msgResult: toastrMessage = { message : null, caption : null};
        this.translate.get(text).subscribe((res: string) => {
            msgResult.message = res;
        });

        if (caption != null)
        {
            this.translate.get(caption).subscribe((res: string) => {
                msgResult.caption = res;
            });
        }

        if (msgResult.message == null)
            msgResult.message = text;

        return msgResult;
    }

    private setDefaults (override?:IndividualConfig): IndividualConfig
    {
        if (override == null)
            override = {} as IndividualConfig;

        if (override.closeButton == null)
            override.closeButton = true;

        return override;
    }

    
}