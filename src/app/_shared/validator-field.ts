export class ValidatorField {
    PropertyName: string;
    Required?: boolean;
    Email?: boolean;
    MinLength?: number;
    MaxLength?: number;
    Pattern?: string;
    CustomValidation: string;
    CustomAsyncValidation: string;
}