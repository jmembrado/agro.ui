import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LiteEvent } from './litle-event'
import { TranslateService } from '@ngx-translate/core';

import { ValidatorField} from './validator-field';


export class ValidatorEngine {

    public Form: FormGroup;
    public fields: ValidatorField[];
    public formErrors = {};

    private readonly onBuilded = new LiteEvent<string>();

    public get Builded() { return this.onBuilded.expose(); }
    
    constructor()
    {
    }

    private transMessages = {};

    public BuildForm(validatorDefinition: ValidatorField[], translate: TranslateService, customValidation?: any, customAsyncValidation?: any): void {
        this.fields = validatorDefinition;

        this.Form = new FormGroup({
            first: new FormControl()
        });

        for (let field of this.fields) {
            let fieldCustomValidation = null;
            let fieldCustomAsyncValidation = null;

            if (customValidation != null && customValidation[field.PropertyName] != null)
                fieldCustomValidation = customValidation[field.PropertyName];

            if (customAsyncValidation != null && customAsyncValidation[field.PropertyName] != null)
                fieldCustomAsyncValidation = customAsyncValidation[field.PropertyName];

            this.addControlField(field, fieldCustomValidation, fieldCustomAsyncValidation);
        }
        this.Form.removeControl("first");

        this.buildMessages(translate);

        this.onBuilded.trigger();

        this.Form.valueChanges.subscribe(data => this.onValueChanged(data));

        this.onValueChanged(); // (re)set validation messages now
    }

    private addControlField(field: ValidatorField, customValidation?: any, customAsyncValidation?: any)
    {
        this.formErrors[field.PropertyName] = "";

        let vals = [];
        if (field.Required) {
            vals.push(Validators.required);
        }
        if (field.Email) {
            vals.push(Validators.email);
        }
        if (field.MinLength) {
            vals.push(Validators.minLength(field.MinLength));
        }
        if (field.MaxLength) {
            vals.push(Validators.maxLength(field.MaxLength));
        }
        if (field.Pattern) {
            vals.push(Validators.pattern(field.Pattern));
        }
        if (field.CustomValidation != null && field.CustomValidation.length > 0 && customValidation != null) {
            vals.push(customValidation);
        }

        this.Form.addControl(field.PropertyName, new FormControl(null, vals, customAsyncValidation));
    }

    public buildMessages(translate: TranslateService)
    {
        if (!this.fields)
            return;

        let messages = {};
        let isInitialized = false;
        
        for (let field of this.fields) {
            if (this.transMessages[field.PropertyName] != null){
                isInitialized = true;
                messages = this.transMessages[field.PropertyName];
            }

            if (field.Required) {
                this.addCommonMessage(messages, translate, 'required');
            }
            if (field.Email) {
                this.addCommonMessage(messages, translate, 'email');
            }
            if (field.MinLength) {
                this.addParametrizedMessage(messages, translate, 'minlength', {value1: field.MinLength.toString()});
            }
            if (field.MaxLength) {
                this.addParametrizedMessage(messages, translate, 'maxlength', {value1: field.MaxLength.toString()});
            }
            if (field.Pattern) {
                this.addCommonMessage(messages, translate, 'pattern');
            }

            if (!isInitialized)
                this.transMessages[field.PropertyName] = messages;
        }
    }

    private addCommonMessage(messages: any, translate: TranslateService, key: string)
    {
        translate.get("validator_" + key).subscribe((res: string) => {
            messages[key] = res;
        });
    }

    private addParametrizedMessage(messages: any, translate: TranslateService, key: string, parameters)
    {
        translate.get("validator_" + key, parameters).subscribe((res: string) => {
            messages[key] = res;
        });
    }

    onValueChanged(data?: any) {
        if (!this.Form) { return; }
        const form = this.Form;

        for (const field in this.formErrors) {
            this.formErrors[field] = "";
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.transMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    isRequired(controlName)
    {
        return this.fields.find(f => f.PropertyName == controlName).Required;
    }

    invalidSubmit() {
        if (!this.Form) { return; }
        const form = this.Form;

        for (const field in this.formErrors) {
            this.formErrors[field] = "";
            const control = form.get(field);

            if (control && !control.valid) {
                const messages = this.transMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }
}