export class ApiResponse {
    ResultCode: string;
    ResultMessage: string;
    Data: any;
}