import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LiteEvent } from './litle-event'


export class TableTools {

    private filterValues = {};
    private exceptFilter: any[];
    private temp: any[];

    public init(data)
    {
      this.temp = [...data];
      return this.applyFilter();
    }

    public addException(field, values) {
      if (this.exceptFilter == null)
        this.exceptFilter = [];
      
      this.exceptFilter.push({field: field, values: values})
    }

    public remove(fieldName: string, value: any)
    {
      let index = this.temp.findIndex(d => d[fieldName] === value);
      if (index >= 0)
        this.temp.splice(index, 1);
    }

    public updateFilter(value, field) {
       
        let val;
        
        if (typeof value === 'string' || value instanceof String)
        {
          val = value.toLowerCase();
          if (val.length == 0)
            val = null;
        }
        else
        {
          val = value;
          if (val == 0)
            val = null;
        }
  
        if (val != null)
          this.filterValues[field] = val;
        else 
          delete this.filterValues[field];
  
        return this.applyFilter();
    }

    public applyFilter()
    {
      let temp = this.temp;

      if (this.exceptFilter != null && this.exceptFilter.length > 0)
      {
        temp = this.temp.filter((d) => {
          let result = true;
          this.exceptFilter.forEach(exception => {
            if (exception.values.findIndex(f => f === d[exception.field]) > -1)
              result = false;
          });
          return result;
        });
      }

      if (Object.keys(this.filterValues).length > 0)
      {
        temp = temp.filter((d) => {
          let result: boolean = false;
          for(var key in this.filterValues) {
            let value = key.split('.').reduce((a, b) => a[b], d);

            //if (typeof value === 'string' || value instanceof String)
            if (typeof value === typeof this.filterValues[key])
            {
              if (typeof value === 'string' || value instanceof String)
              {
                if(value.toLowerCase().indexOf(this.filterValues[key]) !== -1)
                  result = true;
                else
                  result = false;
              }
              else if (typeof value === 'number')
              {
                if (value == this.filterValues[key])
                  result = true;
                else
                  result = false;
              }
              else
              {
                result = false;
              }
            }

            else
              return false;
          }
          return result;
        });
      }
      return temp;
    }
}