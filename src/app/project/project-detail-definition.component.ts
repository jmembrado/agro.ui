import { Component, OnInit, ViewContainerRef, Input, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonModule, Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine, NgSelectTools } from '../_shared';
import { TranslationComponent } from '../translation'

import { Proyect, ProyectLanguage } from '../_models/';

import { AuthService } from '../auth';
import { ProjectService } from './project.service';
import { EntityService } from '../entity';

@Component({
    moduleId: module.id,
    selector: 'agro-project-detail-definition',
    templateUrl: 'project-detail-definition.component.html'
})
export class ProjectDetailDefinitionComponent {

    @Input() id: number;

    private validator: ValidatorEngine;
    private detailsForm: FormGroup;
    
    private isNew: boolean;
    public promiseEdit;

    public data: any;
    public isDataReady: boolean;
    public isFormMetaReady: boolean;
    public isRelatedDataReady: boolean;

    private ratingValue;
    private customers;

    private transColumns;

    @ViewChild(TranslationComponent) translations: TranslationComponent
    
    constructor(
      private logger: NGXLogger,
      private projectService: ProjectService,
      private entityService: EntityService,
      private appServices: AppService,
      private authService: AuthService,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private router: Router,
      private location: Location,
      private route: ActivatedRoute) {

        this.ratingValue = 0;

        this.isDataReady = false;
        this.isFormMetaReady = false;
        this.isRelatedDataReady = false;

        this.validator = new ValidatorEngine();
        this.validator.Builded.on(this.formBuilded);

        this.transColumns = [
            {label: 'Project_Name', prop: 'Name', isRequired: true, isMultiline: false, maxLength: 200},
            {label: 'Project_Description', prop: 'Description', isRequired: false, isMultiline: true, maxLength: 200}]
    }
  
    ngOnInit(): void {

        this.getValidatorDefinition();

        this.getRelatedData();

        this.route.params.subscribe(params => {
            this.doInit(+params['idpj']);
        });
    }

    private doInit(id: number)
    {
        this.id = (isNaN(id) ? null : id);

        this.isNew = (this.id == null);
        this.isDataReady = false;

        if (!this.isNew)
            this.getData();
        else{
            this.data = new Proyect();
            this.data.ProyectLanguage = [];
            this.isDataReady = true;
            this.formLoaded();
        }
    }
  
    private getValidatorDefinition(): void {
        this.projectService.getValidator().then(d => {
            this.validator.BuildForm(d, this.translate);
            this.isFormMetaReady = true;
        });
    }

    private formBuilded = () => {
        this.detailsForm = this.validator.Form;
        this.isFormMetaReady = true;
        this.formLoaded();
    }
      
    private getData() {
        this.projectService.getElement(this.id)
            .then(ar => {
                this.data = ar;
                this.data.Date = new Date(ar.Date);
                this.data.IdEntity = ar.IdEntity.toString();
                this.isDataReady = true;
                this.formLoaded();
        });
    }

    private getRelatedData() {

        this.entityService.getList('cust')
            .then(ar => {
                this.customers = NgSelectTools.arrayToOptionMapper(ar, "IdEntity", "Name");
                this.isRelatedDataReady = true;
                this.formLoaded();
        });

    }

    private formLoaded()
    {
       if (this.isFormMetaReady && this.isDataReady && this.isRelatedDataReady)
       {
            (<FormGroup>this.detailsForm).patchValue(this.data, { onlySelf: true });
            if (this.data != null && this.data.Priority != null)
                this.ratingValue = this.data.Priority;
       }
    }

    public onCancel()
    {
        this.location.back();
    }
  
    public onSubmit() {
        this.toastr.clear();

        if (this.translations.isInvalid())
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("Error_TranslationsInvalid");
            return;    
        }
        if (this.detailsForm.invalid)
        {
            this.toastr.errorTranslate("_ReviewInvalidFileds");
            return;
        }

        this.data = this.detailsForm.value;
        this.data.ProyectLanguage = this.translations.translations;
        this.data.IdEntity = parseInt(this.data.IdEntity);
        this.data.Priority = this.ratingValue;

        if (this.isNew)
        {
            this.data.IdProyect = 0;
            this.doCreate()
        }
        else
        {
            this.data.ProyectLanguage.forEach(m => {
                m.IdProyect = this.id;
            });
            this.data.IdProyect = this.id;
            this.doUpdate()
        }
      
    }

    private doUpdate()
    {
        this.promiseEdit = this.projectService.updateElement(this.data)
        .then(ar => {
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => 
        {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

    private doCreate()
    {
        this.promiseEdit = this.projectService.insertElement(this.data)
        .then(newId => {
            this.isNew = false;
            this.id = newId;
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

}


