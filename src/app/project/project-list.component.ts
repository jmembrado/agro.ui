import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { StarRatingComponent } from 'angular-star-rating';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Proyect } from '../_models';

import { ProjectService } from './project.service'

@Component({
    moduleId: module.id,
    selector: 'agro-project-list',
    templateUrl: 'project-list.component.html'
})
export class ProjectListComponent {

  @Output() select = new EventEmitter();

  private tableTools: TableTools = new TableTools();

  private boolFilter;
  private sub;

  @ViewChild(StarRatingComponent) starRating: StarRatingComponent

  constructor(
    private logger: NGXLogger,
    private projectService: ProjectService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.tableTools = new TableTools();
      
      this.sub = translate.onLangChange.subscribe((event) => {
        this.getBoolFilter();
      })
  }

  ngOnInit() {
    this.getData();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  
  rows = [];
  isLoading = true;
  ratingValue = 0;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  getData()
  {
    this.isLoading = true;

    this.getBoolFilter();

    this.projectService.getList().then(data => { 
        this.rows = this.tableTools.init(data); 
        this.isLoading = false;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }

  private getBoolFilter()
  {
      this.boolFilter = [ //{value:null, label:'' },  
      { value:true, label:null}, 
      { value:false, label:null}];
  
      for (let entry of this.boolFilter) {
        //if (entry.value != null)
        {
          this.translate.get('_' + entry.value).subscribe((res: string) => {
            entry.label = res;
          });
        }
      }
  }

  updateTextFilter(event, field) {
    this.rows = this.tableTools.updateFilter(event.target.value, field);
    this.table.offset = 0;
  }

  clearRatingFilter()
  {
    this.starRating.rating = 0;
    this.ratingValue = 0;
  }

  updateRatingFilter(event) {
    this.ratingValue = event.rating;
    this.rows = this.tableTools.updateFilter(event.rating, 'Priority');
    this.table.offset = 0;
  }

  updateSelectionFilter(event, field) {
    let text = event.target.value;
    this.rows = this.tableTools.updateFilter(text, field);
    this.table.offset = 0;
  }

  onDelete(row)
  {
    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.logger.trace(row.IdEntity);
            this.projectService.deleteElement(row.IdProyect).then(res => {
              let index = this.rows.findIndex(d => d.IdProyect === row.IdProyect);
              this.rows.splice(index, 1);
              this.tableTools.remove("IdProyect", row.IdProyect);
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
            })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
  }

  onSelect(event)
  {
    this.select.emit(event);
  }
}

