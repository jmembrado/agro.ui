import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Template } from '../_models';

import { TemplateService } from './template.service'

@Component({
    moduleId: module.id,
    selector: 'agro-template-list',
    templateUrl: 'template-list.component.html'
})
export class TemplateListComponent {

  @Output() select = new EventEmitter();

  private tableTools: TableTools = new TableTools();

  constructor(
    private logger: NGXLogger,
    private templateService: TemplateService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.tableTools = new TableTools();

  }

  ngOnInit() {
    this.getData();
  }
  
  rows = [];
  isLoading = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  getData()
  {
    this.isLoading = true;
    this.templateService.getList().then(data => { 
        this.rows = this.tableTools.init(data);
        this.isLoading = false;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }

  updateFilter(event, field) {
    this.rows = this.tableTools.updateFilter(event.target.value, field);
    this.table.offset = 0;
  }

  onDelete(row)
  {
    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.logger.trace(row.IdEntity);
            this.templateService.deleteElement(row.IdTemplate).then(res => {
              let index = this.rows.findIndex(d => d.IdTemplate === row.IdTemplate);
              this.rows.splice(index, 1);
              this.tableTools.remove("IdTemplate", row.IdTemplate);
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
            })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
  }

  onSelect(event)
  {
    this.select.emit(event);
  }
}

