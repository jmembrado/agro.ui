import { Component, OnInit, ViewContainerRef, Input, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonModule, Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine, NgSelectTools } from '../_shared';
import { TranslationComponent } from '../translation'

import { Template, TemplateLanguage } from '../_models/';

import { AuthService } from '../auth';
import { TemplateService } from './template.service';
import { PackingService } from '../packing';

@Component({
    moduleId: module.id,
    selector: 'template-detail-definition',
    templateUrl: 'template-detail-definition.component.html'
})
export class TemplateDetailDefinitionComponent {

    @Input() id: number;

    private validator: ValidatorEngine;
    private detailsForm: FormGroup;
    
    private isNew: boolean;
    public promiseEdit;

    public data: any;
    public isDataReady: boolean;
    public isFormMetaReady: boolean;
    public isRelatedDataReady: boolean;

    private packings;
    transColumns;

    @ViewChild(TranslationComponent) translations: TranslationComponent
    
    constructor(
      private logger: NGXLogger,
      private templateService: TemplateService,
      private packingService: PackingService,
      private appServices: AppService,
      private authService: AuthService,
      private fb: FormBuilder,
      private translate: TranslateService,
      public toastr: AppToastrService, 
      private router: Router,
      private location: Location,
      private route: ActivatedRoute) {

        this.isDataReady = false;
        this.isFormMetaReady = false;
        this.isRelatedDataReady = true;

        this.validator = new ValidatorEngine();
        this.validator.Builded.on(this.formBuilded);

        this.transColumns = [{label: 'Template_Name', prop: 'Name', isRequired: true, isMultiline: false, maxLength: 200}];
    }
  
    ngOnInit(): void {

        this.getValidatorDefinition();

        let sub = this.route.params.subscribe(params => {
            this.doInit(+params['idt']);
        });
    }

    private doInit(id: number)
    {
        this.id = (isNaN(id) ? null : id);

        this.isNew = (this.id == null);
        this.isDataReady = false;

        if (!this.isNew)
            this.getData();
        else{
            this.data = new Template();
            this.data.TemplateLanguage = [];
            this.isDataReady = true;
            this.formLoaded();
        }
    }
  
    private getValidatorDefinition(): void {
        this.templateService.getValidator().then(d => {
            this.validator.BuildForm(d, this.translate);
            this.isFormMetaReady = true;
        });
    }

    private formBuilded = () => {
        this.detailsForm = this.validator.Form;
        this.isFormMetaReady = true;
        this.formLoaded();
    }
      
    private getData() {
        this.templateService.getElement(this.id)
            .then(ar => {
                this.data = ar;
                this.isDataReady = true;
                this.formLoaded();
        });
    }

    private formLoaded()
    {
       if (this.isFormMetaReady && this.isDataReady && this.isRelatedDataReady)
       {
            (<FormGroup>this.detailsForm).patchValue(this.data, { onlySelf: true });
       }
    }

    public onCancel()
    {
        this.location.back();
    }
  
    public onSubmit() {
        this.toastr.clear();

        if (this.translations.isInvalid())
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("Error_TranslationsInvalid");
            return;    
        }
        if (this.detailsForm.invalid)
        {
            this.toastr.errorTranslate("_ReviewInvalidFileds");
            return;
        }

        this.data = this.detailsForm.value;
        this.data.TemplateLanguage = this.translations.translations;

        if (this.isNew)
        {
            this.data.IdTemplate = 0;
            this.doCreate()
        }
        else
        {
            this.data.TemplateLanguage.forEach(m => {
                m.IdTemplate = this.id;
            });
            this.data.IdTemplate = this.id;
            this.doUpdate()
        }
      
    }

    private doUpdate()
    {
        this.promiseEdit = this.templateService.updateElement(this.data)
        .then(ar => {
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => 
        {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

    private doCreate()
    {
        this.promiseEdit = this.templateService.insertElement(this.data)
        .then(newId => {
            this.isNew = false;
            this.id = newId;
            this.toastr.successTranslate("_DataSuccessfullySaved");
            this.location.back();
        })
        .catch(err => {
            if (err)
                this.toastr.errorTranslate(err);
        });
    }

}


