import { Component, OnInit, ViewChild, Input, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { ITreeOptions, TreeNode, TREE_ACTIONS, IActionMapping, ITreeState, TreeComponent } from 'angular-tree-component';
import * as _ from 'lodash';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';

import { Template } from '../_models';

import { TemplateTreeService } from './template-tree.service'
import { TemplateTreeComponent } from './template-tree.component'

@Component({
    moduleId: module.id,
    selector: 'agro-template-tree-editor',
    templateUrl: 'template-tree-editor.component.html'
})
export class TemplateTreeEditorComponent {

  productUnits: number;
  templateUnits: number;
  selectedProduct;
  selectedTemplate;

  promiseAction;

  editingUnits:number;
  currentAction:string;
  currentNode;

  //@ViewChild(TreeComponent) tree: TreeComponent
  @ViewChild(TemplateTreeComponent) templateTree: TemplateTreeComponent

  constructor(
    private logger: NGXLogger,
    private templateTreeService: TemplateTreeService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.productUnits = 1;
      this.templateUnits = 1;
  }

  ngOnInit() {
  }

  onProductSelected(event)
  {
    this.selectedProduct = event.selected[0];
  }
  
  onTemplateSelected(event)
  {
    this.selectedTemplate = event.selected[0];
  }

  onAddProduct()
  {
    let hasError = false;
    if (this.productUnits <= 0){
      this.toastr.errorTranslate('Error_TemplateUnitsUnderZero');
      hasError = true;
    }

    if (this.selectedProduct == null){
      this.toastr.errorTranslate('Error_TemplateMustSelectOneProduct');
      hasError = true;
    }

    //if (this.currentAction == 'adding' && this.currentNode != null )
    // if (this.state == null || this.state["focusedNodeId"] == null){
    //   this.toastr.errorTranslate('Error_TemplateMustSelectOneTreeTemplate');
    //   hasError = true;
    // }

    if (!hasError)
    {
      //let sTemplateParentId = this.state.focusedNodeId.toString();
      let sTemplateParentId = this.currentNode.data.id;
      let idParentTemplate = this.templateTree.getChildren(sTemplateParentId);
      let sIdChildTemplate:string = idParentTemplate.toString() + this.templateTree._SEP + "t-" + this.selectedTemplate.IdTemplate.toString();

      let sIdProduct:string = idParentTemplate.toString() + this.templateTree._SEP + "p-" + this.selectedProduct.IdProduct.toString();
      this.promiseAction = this.templateTreeService.insertChild('product', sTemplateParentId, sIdProduct, this.productUnits)
      .then(p =>
        {
          let newNode = {type: 'product', name : this.selectedProduct.Description, Units: this.productUnits, isLeaf : true}
          this.templateTree.addNode(newNode);
          this.toastr.successTranslate("_DataSuccessfullySaved");
        });
    }
  }

  onAddTemplate()
  {
    let hasError = false;
    if (this.templateUnits <= 0){
      this.toastr.errorTranslate('Error_TemplateUnitsUnderZero');
      hasError = true;
    }

    if (this.selectedTemplate == null){
      this.toastr.errorTranslate('Error_TemplateMustSelectOneProduct');
      hasError = true;
    }

    // if (this.state == null || this.state["focusedNodeId"] == null){
    //   this.toastr.errorTranslate('Error_TemplateMustSelectOneTreeTemplate');
    //   hasError = true;
    // }

    if (!hasError)
    {
      //let sTemplateParentId = this.state.focusedNodeId.toString();
      let sTemplateParentId = this.currentNode.data.id;
      let idParentTemplate = this.templateTree.getElementId(sTemplateParentId);
      let sIdChildTemplate:string = idParentTemplate.toString() + this.templateTree._SEP + "t-" + this.selectedTemplate.IdTemplate.toString();

      this.promiseAction = this.templateTreeService.insertChild('template', sTemplateParentId, sIdChildTemplate, this.templateUnits)
      .then(p =>
        {
          let newNode = {type: 'template', name : this.selectedTemplate.Name, Units: this.templateUnits, isLeaf : false};
          this.templateTree.addNode(newNode);

          this.toastr.successTranslate("_DataSuccessfullySaved");
        });
    }
  }

  templateTreeOnEdit(node)
  {
    this.currentAction = 'editing';
    this.editingUnits = node.data.Units;
    this.currentNode = node;
  }

  templateTreeOnAdd(node)
  {
    this.currentAction = 'adding';
    this.currentNode = node;
  }

  templateTreeOnDeleted(nodeId)
  {
    if (this.currentNode != null && this.currentNode.data.id == nodeId)
    {
      this.currentNode = null;
      this.currentAction = null;
    }
  }

  updateEditingUnits(event)
  {
    this.editingUnits = parseInt(event.target.value);
  }

  updateProductUnits(event)
  {
    this.productUnits = parseInt(event.target.value);
  }

  updateTemplateUnits(event)
  {
    this.templateUnits = parseInt(event.target.value);
  }

  onSave()
  {
    let node = this.currentNode;
    this.promiseAction = this.templateTreeService.updateChild(node.data.type, node.parent.data.id, node.data.id, this.editingUnits)
    .then(p =>
      {
        this.templateTree.editNode(this.editingUnits);

        this.toastr.successTranslate("_DataSuccessfullySaved");
      });
  }

}

