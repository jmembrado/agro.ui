import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { ITreeOptions, TreeNode, TREE_ACTIONS, IActionMapping, ITreeState, TreeComponent, KEYS } from 'angular-tree-component';
import * as _ from 'lodash';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';

import { Template } from '../_models';

import { TemplateTreeService } from './template-tree.service'

@Component({
    moduleId: module.id,
    selector: 'agro-template-tree',
    templateUrl: 'template-tree.component.html'
})
export class TemplateTreeComponent {
  
  @Input() mode: string;
  @Output() onEdit = new EventEmitter();
  @Output() onAdd = new EventEmitter();
  @Output() onDeleted = new EventEmitter();
  @Input() promiseAction;
  @Input() currentAction;

  private editingNode: TreeNode;
  private addingNode: TreeNode;
      
  public isLoading = true;
  private state: ITreeState;
  private nodes = [];

  private options: ITreeOptions;

  @ViewChild(TreeComponent) tree: TreeComponent

  constructor(
    private logger: NGXLogger,
    private templateTreeService: TemplateTreeService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      
  }

  ngOnInit() {

    if (this.mode == 'selector')
    {
      this.options = {
        actionMapping: {
          mouse: {
            click: TREE_ACTIONS.TOGGLE_SELECTED
          }
        },
        getChildren: this.getChildren.bind(this)
      }
    }
    else
    {
      this.options = {
        actionMapping: {
          mouse: {
            click: null
          },
          keys: {
            [KEYS.RIGHT]: null,
            [KEYS.LEFT]: null,
            [KEYS.DOWN]: null,
            [KEYS.UP]: null,
            [KEYS.SPACE]: null,
            [KEYS.ENTER]: null
          }
        },
        getChildren: this.getChildren.bind(this)
      }
    }

    this.getData();
  }

  private getData()
  {
    this.isLoading = true;
    this.templateTreeService.getRoot().then(data => { 
        this.nodes = data;
        this.isLoading = false;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }

  public addNode(newNode)
  {
    //let node = this.tree.treeModel.getNodeById()
    this.addingNode.data.children.push(newNode);
    this.updateTree();
  }

  public editNode(units: number)
  {
    //let node = this.tree.treeModel.getNodeById()
    //let node = this.addingNode;
    //node.data.children.push(newNode);
    this.editingNode.data.Units = units;
    this.tree.treeModel.update();
  }

  private updateTree()
  {
    this.nodes = [].concat(this.nodes);
    

    // this.addingNode = null;
    // this.editingNode = null;
    // this.currentAction = '';
  }

  getChildren(node: TreeNode) {
    return new Promise((resolve, reject) => {
      this.templateTreeService.getChildren(node.id).then(n => 
        {
          this.updateTree();
          return resolve(n);
        }
      );
    });
  }

  private onNodeEdit(node)
  {
    this.editingNode = node;
    this.addingNode = null;
    //this.currentAction = 'editing';
    this.onEdit.emit(node);
  }

  private onNodeAdd(node)
  {
    this.addingNode = node;
    this.editingNode = null;
    //this.currentAction = 'adding';
    this.onAdd.emit(node);
  }

  private onNodeDelete(node:TreeNode)
  {
    this.currentAction = 'deleting';

    this.promiseAction = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.templateTreeService.deleteChild(node.data.type, node.parent.data.id, node.data.id).then(res => {
              let nodeId = node.data.id;
              
              _.remove(node.parent.data.children, node.data);

              this.updateTree();

              this.toastr.successTranslate("Delete_ConfirmMessage");

              this.onDeleted.emit(nodeId);
            })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
  }

  public _SEP: string = "*";
  public getElementId(sId:string): number
  {
    let a:number = sId.indexOf(this._SEP);
    return parseInt(sId.substring(a)+3);
  }
}


