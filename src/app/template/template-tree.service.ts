import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NGXLogger } from 'ngx-logger';

import { Template, Language } from '../_models';

import { ApiResponse, ValidatorField } from '../_shared';
import { AuthService } from '../auth'
import { LanguageService } from '../translation'

@Injectable()
export class TemplateTreeService {

    private serviceUrl = '/api/TemplatesTree';

    constructor(private logger: NGXLogger,
        private http: Http,
        private authService: AuthService,
        private languageService: LanguageService) { }

    private handleError(error: any): Promise<any> {
        if (error.status)
        {
            switch (error.status)
            {
                case 401:
                    this.authService.found401();
                    return Promise.resolve("Error_401"); 
                case 400:
                    if (error._body != null && error._body.length > 0)
                    {
                        let respErr = JSON.parse(error._body) as ApiResponse;
                        return Promise.reject(respErr.ResultCode);
                    }
                    else
                    {
                        this.logger.error(error);
                        return error;
                    }                    
            }

        }

        this.logger.error(error);
            
        return Promise.reject(error.message || error);
    }

    public getRoot(): Promise<any[]> {
        return this.languageService.getCurrentLanguageId().then(idLanguage => 
                this.getRootByIdLanguage(idLanguage))
    }

    private getRootByIdLanguage(idLanguage:number): Promise<any[]> {
        const url = `${this.serviceUrl}/root/${idLanguage}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public getChildren(idTemplate:string): Promise<any[]> {
        return this.languageService.getCurrentLanguageId().then(idLanguage => 
                this.getChildrenByIdLanguage(idLanguage, idTemplate))
    }

    private getChildrenByIdLanguage(idLanguage:number, idTemplate:string): Promise<any[]> {
        const url = `${this.serviceUrl}/children/${idLanguage}/${idTemplate}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public updateChild(type:string, idParent:string, idProduct:string, unit:number): Promise<number> {
        const url = `${this.serviceUrl}/${type}/${idParent}/${idProduct}/${unit}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.put(url, "", { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public insertChild(type:string, idParent:string, idProduct:string, unit:number): Promise<number> {
        const url = `${this.serviceUrl}/${type}/${idParent}/${idProduct}/${unit}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.post(url, "", { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public deleteChild(type:string, idParent:string, idProduct:string): Promise<any> {
        const url = `${this.serviceUrl}/${type}/${idParent}/${idProduct}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.delete(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
}