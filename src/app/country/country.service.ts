import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NGXLogger } from 'ngx-logger';

import { Country } from '../_models';

import { ApiResponse, ValidatorField } from '../_shared';
import { AuthService } from '../auth'

@Injectable()
export class CountryService {

    private serviceUrl = '/api/Countries';
    
    constructor(private logger: NGXLogger,
        private http: Http,
        private authService: AuthService) { }

    private handleError(error: any): Promise<any> {
        if (error.status)
        {
            switch (error.status)
            {
                case 401:
                    this.authService.found401();
                    return Promise.resolve("Error_401"); 
                case 400:
                    if (error._body != null && error._body.length > 0)
                    {
                        let respErr = JSON.parse(error._body) as ApiResponse;
                        return Promise.reject(respErr.ResultCode);
                    }
                    else
                    {
                        this.logger.error(error);
                        return error;
                    }                    
            }

        }

        this.logger.error(error);
            
        return Promise.reject(error.message || error);
    }

    public getValidator(): Promise<ValidatorField[]> {
        const url = `api/Validator/Country`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => {
                return response.json() as ValidatorField[];
            })
            .catch(this.handleError);
    }

    private countriesCache: Country[];


    public getListToOptionMapper(idDefaultLang: number, idLang: number): Promise<any[]>
    {
        return this.getList().then(countries => {
            let res = [];
            countries.forEach(element => {
                if (element.CountryLanguage != null)
                {
                    let langIndex = element.CountryLanguage.findIndex(d => d.IdLanguage === idLang);
                    if (langIndex == -1)
                        langIndex = element.CountryLanguage.findIndex(d => d.IdLanguage === idDefaultLang);
                    res.push({value: element.IdCountry.toString(), label: element.CountryLanguage[langIndex].Name});    
                }
            });
            res.sort((a,b) => (a.label > b.label ? 1 : -1 ));
            return res;
        });
    }

    public getList(): Promise<Country[]> {
        // if (this.countriesCache != null)
        //     return new Promise(resolve => {
        //         resolve(this.countriesCache);
        //     });

        let url = `${this.serviceUrl}/`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => 
                {
                    this.countriesCache = response.json();
                    return this.countriesCache;
                })
            .catch(this.handleError);
    }

    public getElement(idCountry: number): Promise<Country> {
        const url = `${this.serviceUrl}/${idCountry}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public updateElement(element: Country): Promise<any> {
        const url = `${this.serviceUrl}/${element.IdCountry}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.put(url, JSON.stringify(element), { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public insertElement(element: Country): Promise<number> {
        const url = `${this.serviceUrl}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.post(url, JSON.stringify(element), { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public deleteElement(id: number): Promise<any> {
        const url = `${this.serviceUrl}/${id}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.delete(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

}