import { Component, Input, Output, EventEmitter  } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } 
from 'ngx-uploader';

import { Attach } from '../_models'

import { Headers, Http, RequestOptions } from '@angular/http';
import { AuthService } from '../auth'


@Component({
  selector: 'agro-attach-upload',
  templateUrl: 'attach-upload.component.html'
})
export class AttachUploadComponent {
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  options: UploaderOptions;

  @Input() type: string;
  @Input() idSource: number;
  @Output() onUploaded = new EventEmitter();

  constructor(private http: Http, private authService: AuthService) {
    this.options = { concurrency: 1, allowedContentTypes: 
        //["image/jpeg", 'image/gif', "image/png", "application/pdf", "application/msword", "application/zip"]
        ['*']
    };
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
  }


  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {
      const event: UploadInput = {
        type: 'uploadAll',
        url: `api/Attaches/${this.type}/${this.idSource}`,
        withCredentials: true,
        headers: {Authorization: "Bearer " + this.authService.getToken() }
      };

      this.uploadInput.emit(event);
    } else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') {
      this.files.push(output.file);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
      this.dragOver = false;
    } else if (output.type === 'drop') {
      this.dragOver = false;
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      console.log(output.file.name + ' rejected');
    }

    this.files = this.files.filter(file => file.progress.status !== UploadStatus.Done);
    this.onUploaded.emit();
  }

}