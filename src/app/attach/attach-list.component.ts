import { Component, OnInit, ViewChild, Input, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {saveAs as importedSaveAs} from "file-saver";

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { TableTools } from '../_shared';

import { Attach } from '../_models';

import { AttachService } from './attach.service'

@Component({
    moduleId: module.id,
    selector: 'agro-attach-list',
    templateUrl: 'attach-list.component.html'
})
export class AttachListComponent {

  public type: string;
  public idSource: number;

  private tableTools: TableTools = new TableTools();

  constructor(
    private logger: NGXLogger,
    private attachService: AttachService,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private route: ActivatedRoute,
    private router: Router) {
      this.tableTools = new TableTools();

  }

  ngOnInit() {

    let sub = this.route.params.subscribe(params => {
      this.type = params['attype'];
      this.idSource = +params['idsrc'];
      
      this.getData();
  });
    
  }
  
  rows = [];
  isLoading = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  public getData()
  {
    this.isLoading = true;
    this.attachService.getList(this.type, this.idSource).then(data => { 
        this.rows = this.tableTools.init(data);
        this.isLoading = false;
      })
    .catch(err => {
        this.toastr.errorTranslate("Error_Connection");
        this.isLoading = false;
      })
  }

  updateFilter(event, field) {
    this.rows = this.tableTools.updateFilter(event.target.value, field);
    this.table.offset = 0;
  }

  onDelete(row)
  {
    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
            this.logger.trace(row.IdEntity);
            this.attachService.deleteElement(this.type, this.idSource, row.IdAttach).then(res => {
              let index = this.rows.findIndex(d => d.IdAttach === row.IdAttach);
              this.rows.splice(index, 1);
              this.tableTools.remove("IdAttach", row.IdAttach);
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
            })
            .catch(err => {
              this.toastr.errorTranslate(err);
            })
          }
      });
  }

  onDownloadFile(row)
  {
    this.attachService.dowloadElement(this.type, this.idSource, row.IdAttach).subscribe(blob => {
          importedSaveAs(blob, row.Title);
      })
  }

  getIconByMimeType(mimeType:string)
  {

    if (mimeType.startsWith('image'))
    {
      return 'fa-file-image-o';
    }
    else if (mimeType.startsWith('audio'))
    {
      return 'fa-file-audio-o';
    }
    else if (mimeType.startsWith('video'))
    {
      return 'fa-file-video-o';
    }
    else
    {
      switch (mimeType)
      {
        case 'application/pdf':
          return 'fa-file-pdf-o';
        case 'text/plain':
          return 'fa-file-text-o';
        case 'text/html':
          return 'fa-file-code-o';
        case 'application/gzip':
        case 'application/zip':
          return 'fa-file-archive-o';
      }
    }
  }
}

