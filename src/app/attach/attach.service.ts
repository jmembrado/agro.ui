import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Headers, Http, RequestOptions, ResponseContentType } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NGXLogger } from 'ngx-logger';

import { Attach } from '../_models';

import { ApiResponse, ValidatorField } from '../_shared';
import { AuthService } from '../auth'

@Injectable()
export class AttachService {

    private serviceUrl = '/api/Attaches';

    constructor(private logger: NGXLogger,
        private http: Http,
        private authService: AuthService) { }

    private handleError(error: any): Promise<any> {
        if (error.status)
        {
            switch (error.status)
            {
                case 401:
                    this.authService.found401();
                    return Promise.resolve("Error_401"); 
                case 400:
                    if (error._body != null && error._body.length > 0)
                    {
                        let respErr = JSON.parse(error._body) as ApiResponse;
                        return Promise.reject(respErr.ResultCode);
                    }
                    else
                    {
                        this.logger.error(error);
                        return error;
                    }                    
            }

        }

        this.logger.error(error);
            
        return Promise.reject(error.message || error);
    }

    public getValidator(): Promise<ValidatorField[]> {
        const url = `api/Validator/Template`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => {
                return response.json() as ValidatorField[];
            })
            .catch(this.handleError);
    }

    public getList(type:string, idSource:number): Promise<any[]> {
        const url = `${this.serviceUrl}/${type}/${idSource}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    public dowloadElement(type:string, idSource:number, id: number): Observable<Blob> {
        const url = `${this.serviceUrl}/download/${type}/${idSource}/${id}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        let options = new RequestOptions({responseType: ResponseContentType.Blob, headers: authHeaders});
        return this.http.get(url, options)
            .map(res => res.blob())
            .catch(this.handleError)
    }

    public deleteElement(type:string, idSource:number, id: number): Promise<any> {
        const url = `${this.serviceUrl}/${type}/${idSource}/${id}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.delete(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
}