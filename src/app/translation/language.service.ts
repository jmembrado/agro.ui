import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, ValidatorField } from '../_shared';
import { AuthService } from '../auth'

import { Language } from '../_models';

@Injectable()
export class LanguageService {

    private serviceUrl = '/api/Languages';
    
    constructor(private logger: NGXLogger,
        private http: Http,
        private authService: AuthService,
        private translate: TranslateService) { }

    private handleError(error: any): Promise<any> {
        if (error.status)
        {
            switch (error.status)
            {
                case 401:
                    this.authService.found401();
                    return Promise.resolve("Error_401"); 
                case 400:
                    if (error._body != null && error._body.length > 0)
                    {
                        let respErr = JSON.parse(error._body) as ApiResponse;
                        return Promise.reject(respErr.ResultCode);
                    }
                    else
                    {
                        this.logger.error(error);
                        return error;
                    }                    
            }

        }

        this.logger.error(error);
            
        return Promise.reject(error.message || error);
    }

    public defautlLanguage: number = 1;

    public getCurrentLanguageId(): Promise<number> {
        return this.getLangId(this.translate.currentLang);
    }

    public getLangId(code: string):Promise<number>
    {
        return new Promise(resolve => {this.getList().then( langs => {
            let idLang: number = langs.find(l => l.Code == code).IdLanguage;
            resolve(idLang);
            });
        });
    }

    public getList(): Promise<Language[]> {
        const url = `${this.serviceUrl}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            //.map((res:Response) => res.json().friends)
            //.publishReplay(1)
            //.refCount()
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }


}