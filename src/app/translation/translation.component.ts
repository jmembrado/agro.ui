import { Component, ViewChild, Input, TemplateRef, OnInit } from '@angular/core';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';

import { TranslationResponse, TranslationRequest } from '../_models';

import { LanguageService } from './language.service';
import { TranslationService } from './translation.service';
import { jsonpCallbackContext } from '@angular/common/http/src/module';


export class TranslationColumn{
  label: string;
  prop: string;
  isRequired: boolean;
  isMultiline: boolean;
  maxLength: number;
}

@Component({
  moduleId: module.id,
  selector: 'agro-translation',
  templateUrl: 'translation.component.html'
})
export class TranslationComponent {

  @Input() columns: TranslationColumn[];

  @Input('translations') get translations() {
    return this.rows;
  }
  set translations(translations) {
    if (translations == null)
    {
      this.rows = [];
    }
    else
    {
      this.rows = translations;
      this.fillRemainingLangs();
      this.isLoading = false;
    }
  }

  public isInvalid(): boolean {
    if (this.rows.length == 0)
      return true;

    let isInvalid = false;
    let index = this.rows.findIndex(d => d.IdLanguage === this.languageService.defautlLanguage);
    if (index >= 0) {
      this.columns.forEach(element => {
        if (element.isRequired && (this.rows[index][element.prop] == null || this.rows[index][element.prop].trim().length == 0))
        isInvalid = true;
      });
      return isInvalid;
    }
    return true;
  }
  public isLoading: boolean;
  public isRelatedDataReady: boolean;
  private promiseTranslate;
      
  public rows = [];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  private allLangs = [];
  public remainingLangs = [];
  public autoTransLangs = {};

  constructor(
    private logger: NGXLogger,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private languageService: LanguageService,
    private translationService: TranslationService
  ) {
    this.isLoading = true;
    this.isRelatedDataReady = false;

    if (this.columns == null)
    {
      this.columns = [{label: 'Translation_Description', prop: 'Description', isRequired: true, isMultiline: false, maxLength: 200}]
    }
  }

  private doInit()
  {
    
  }

  ngOnInit() {

    this.getLanguages();
  }

  fillRemainingLangs()
  {
    if (this.rows == null)
      return;

    this.remainingLangs = [...this.allLangs];

    this.rows.forEach((element, index) => {
      let delIndex = this.remainingLangs.findIndex(d => d.IdLanguage === element.IdLanguage);
      this.remainingLangs.splice(delIndex, 1);

      this.columns.forEach(field => 
      {
        this.fillAutoTrans(field.prop, element.IdLanguage);
      });
    });
  }

  fillAutoTrans(Prop: string, IdLanguage: number)
  {
    if (this.autoTransLangs[Prop] == null)
      this.autoTransLangs[Prop] = {};

    if (this.autoTransLangs[Prop][IdLanguage.toString()] == null || this.autoTransLangs[Prop][IdLanguage.toString()].Auto.length == 0)
    {
      let allOtherLangs = [];
      this.allLangs.forEach(l => {
        if (l.IdLanguage != IdLanguage){
          allOtherLangs.push( {IdLanguage: l.IdLanguage, Active: true} );
        }
      });
      this.autoTransLangs[Prop][IdLanguage.toString()] = { Expanded: false, Auto: allOtherLangs};
    }
  }

  getLanguageDesc(id:number): string
  {
    if (!this.isRelatedDataReady)
      return "";

    return this.allLangs.find(l => l.IdLanguage === id).Name;
  }

  private getLanguages()
  {
    this.languageService.getList().then(data => { 
         this.allLangs = [...data];
         this.isRelatedDataReady = true;
         this.fillRemainingLangs();
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
        })
  }

  onDelete(row)
  {
    if (row.IdLanguage ==  this.languageService.defautlLanguage)
    {
      this.toastr.errorTranslate("Error_TranslationsInvalid"); 
      return;
    }


    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
              let index = this.rows.findIndex(d => d.IdLanguage === row.IdLanguage);
              this.rows.splice(index, 1);
              //this.rows = [...this.rows];
              this.fillRemainingLangs();

              this.toastr.successTranslate("Delete_ConfirmMessage"); 
          }
      });
  }

  updateTextValue(event, prop, rowIndex) {
    this.rows[rowIndex][prop] = event.target.value;
    this.rows = [...this.rows];
  }

  onLangChange(event, rowIndex){
    this.rows[rowIndex]["IdLanguage"] = parseInt(event.target.value);
    this.rows = [...this.rows];
    this.fillRemainingLangs();
  }

  onAdd(event)
  {
    if (this.remainingLangs.length > 0)
    {
      let id = this.remainingLangs[0].IdLanguage;
      this.rows.push({ IdLanguage : id, Description : '' });
      this.rows = [...this.rows];
      this.fillRemainingLangs();
    }
      
  }

  onTranslate(idLanguage, text, prop)
  {
    if (text.length == 0)
    {
      this.toastr.errorTranslate("Error_TextEmpty"); 
      return;
    }

    let destLangs:number[] = []
    this.autoTransLangs[prop][idLanguage].Auto.forEach(element => {
      if (element.Active)
      {
        destLangs.push(element.IdLanguage);
      }
    });

    let request: TranslationRequest = {
      IdSourceLanguage : idLanguage,
      Text: text,
      Prop: prop,
      DestLanguages: destLangs
    };

    this.promiseTranslate = this.translationService.translate(request)
      .then(resp => {
        resp.forEach(tLang => {
          if (tLang.Success)
          {
            let index = this.rows.findIndex(d => d.IdLanguage === tLang.IdLanguage);
            if (index >= 0) {
              this.rows[index][tLang.Prop] = tLang.Text;
            }
            else {
              index = this.rows.push({ IdLanguage : tLang.IdLanguage });
              this.rows[index-1][tLang.Prop] = tLang.Text;
            }
          }
        });
        this.rows = [...this.rows];

        if (resp.length > 0)
          this.fillRemainingLangs();
      });
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onActiveLangChange(event, al){
    al.Active = event.target.checked;
  }

}

