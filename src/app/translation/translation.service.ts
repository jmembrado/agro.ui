import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NGXLogger } from 'ngx-logger';

import { TranslationResponse, TranslationRequest } from '../_models';

import { ApiResponse, ValidatorField } from '../_shared';
import { AuthService } from '../auth'

@Injectable()
export class TranslationService {

    private serviceUrl = '/api/Translator';
    
    constructor(private logger: NGXLogger,
        private http: Http,
        private authService: AuthService) { }

    private handleError(error: any): Promise<any> {
        if (error.status)
        {
            switch (error.status)
            {
                case 401:
                    this.authService.found401();
                    return Promise.resolve("Error_401"); 
                case 400:
                    if (error._body != null && error._body.length > 0)
                    {
                        let respErr = JSON.parse(error._body) as ApiResponse;
                        return Promise.reject(respErr.ResultCode);
                    }
                    else
                    {
                        this.logger.error(error);
                        return error;
                    }
                case 500:
                    return Promise.reject("Error_500");
            }

        }

        this.logger.error(error);
            
        return Promise.reject(error.message || error);
    }


    public translate(translation: TranslationRequest): Promise<TranslationResponse[]> {
        const url = `${this.serviceUrl}/`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.post(url, JSON.stringify(translation), { headers: authHeaders })
            .toPromise()
            .then(response => response.json() as TranslationResponse[])
            .catch(this.handleError);
    }
}