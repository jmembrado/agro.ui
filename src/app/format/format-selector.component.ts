import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';

import { AppToastrService } from '../_shared';

import { FormatLength, FormatWeight } from '../_models';

import { FormatService } from './format.service'

@Component({
    moduleId: module.id,
    selector: 'agro-format-selector',
    templateUrl: 'format-selector.component.html'
})
export class FormatSelectorComponent {

  @Input() type: string;

  @Input() control: FormControl;

  @ViewChild('selector') inputRef: ElementRef;
  
  public formats: any[];
  
  constructor(
    private logger: NGXLogger,
    private formatService: FormatService,
    public toastr: AppToastrService) {
      this.type = "Length"
  }

  ngOnInit() {
    this.getData();
  }
  
  isLoading = true;

  private getData()
  {
    this.isLoading = true;
    this.formatService.getList(this.type).then(data => { 
        this.isLoading = false;
        this.formats = data;
      })
    .catch(err => {
          this.toastr.errorTranslate("Error_Connection");
          this.isLoading = false;
        })
  }
}