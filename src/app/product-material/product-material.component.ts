import { Component, ViewChild, Input, TemplateRef, OnInit } from '@angular/core';

import { NGXLogger } from 'ngx-logger';
import { TranslateService } from '@ngx-translate/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

import { AppConfirmComponent } from '../_components';
import { DialogService } from "ng2-bootstrap-modal";
import { AppToastrService } from '../_shared';
import { MaterialService } from '../material';

@Component({
    moduleId: module.id,
    selector: 'product-material',
    templateUrl: 'product-material.component.html'
})
export class ProductMaterialComponent {

  @Input() idProduct: number;

  private isNew: boolean;

  @Input('productMaterial') get productMaterial() {
    return this.rows;
  }
  set productMaterial(value) {
    if (value == null)
    {
      this.rows = [];
      this.isNew = true;
    }
    else
    {
      this.rows = value;
      this.isDataReady = true;
      this.formLoaded();
    }
  }

  public isInvalid(): boolean {
    if (this.rows.length == 0)
      return false;

    let total = 0
    this.rows.forEach(el => {
      total += el["Percentage"];
    });

    if (total > 100){
      this.toastr.errorTranslate("Error_TotalProductMaterial100");
    }

  }

  public isLoading: boolean;
  public isDataReady: boolean;
  public isRelatedDataReady: boolean;
  private promiseTranslate;
      
  public rows = [];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public remainingMaterials = [];
  private allMaterials = [];

  constructor(
    private logger: NGXLogger,
    private translate: TranslateService,
    public toastr: AppToastrService, 
    private dialogService: DialogService,
    private materialService: MaterialService
  ) {
    this.isLoading = true;
    this.isDataReady = false;
    this.isRelatedDataReady = false;
  }

  ngOnInit() {

    this.materialService.getList()
      .then(data => { 
        this.allMaterials = [...data];
        this.isRelatedDataReady = true;
        this.formLoaded();
      })
      .catch(err => {
        this.toastr.errorTranslate("Error_Connection");
      });
  }

  private formLoaded()
  {
      if (this.isDataReady && this.isRelatedDataReady)
      {
        this.isLoading = false;
        this.fillRemainingMaterials();
      }
  }

  fillRemainingMaterials()
  {
    this.remainingMaterials = [...this.allMaterials];

    this.rows.forEach((element, index) => {
      let delIndex = this.remainingMaterials.findIndex(d => d.IdMaterial === element.IdMaterial);
      this.remainingMaterials.splice(delIndex, 1);
    });
  }

  onDelete(row)
  {
    let disposable = this.dialogService.addDialog(AppConfirmComponent, {
      title:'Delete_Title', 
      message:'Delete_ConfirmQuestion'})
      .subscribe((isConfirmed)=>{
          if(isConfirmed) {
              let index = this.rows.findIndex(d => d.IdMaterial === row.IdMaterial);
              this.rows.splice(index, 1);
              
              this.toastr.successTranslate("Delete_ConfirmMessage"); 
          }
      });
  }

  updateTextValue(event, rowIndex) {
    if (event.target.value.length == 0)
      return;

    let percentage: number;
    try {
      percentage = parseInt(event.target.value);
    } catch (error) {
        this.toastr.errorTranslate("Error_ValueMustBeInteger");
        return;
    }

    if (percentage <= 0)
      this.toastr.errorTranslate("Error_ValueMustBeBigger0");

    this.rows[rowIndex]["Percentage"] = percentage;
    this.rows = [...this.rows];
  }

  onMaterialChange(event, rowIndex){
    this.rows[rowIndex]["IdMaterial"] = parseInt(event.target.value);
    this.rows = [...this.rows];
    this.fillRemainingMaterials();
  }

  onAdd(event)
  {
    if (this.remainingMaterials.length > 0)
    {
      let id = this.remainingMaterials[0].IdMaterial;
      this.rows.push({ IdMaterial : id });
      this.fillRemainingMaterials();
    }
  }

  getMaterialDesc(id:number): string
  {
    if (!this.isRelatedDataReady)
      return "";
    return this.allMaterials.find(l => l.IdMaterial === id).Description;
  }

}

