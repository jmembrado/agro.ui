import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { NGXLogger } from 'ngx-logger';

import { ProductMaterial } from '../_models';

import { ApiResponse, ValidatorField } from '../_shared';
import { AuthService } from '../auth'
import { LanguageService } from '../translation'

@Injectable()
export class ProductMaterialService {

    private serviceUrl = '/api/ProductMaterials';

    constructor(private logger: NGXLogger,
        private http: Http,
        private authService: AuthService,
        private languageService: LanguageService) { }

    private handleError(error: any): Promise<any> {
        if (error.status)
        {
            switch (error.status)
            {
                case 401:
                    this.authService.found401();
                    return Promise.resolve("Error_401"); 
                case 400:
                    if (error._body != null && error._body.length > 0)
                    {
                        let respErr = JSON.parse(error._body) as ApiResponse;
                        return Promise.reject(respErr.ResultCode);
                    }
                    else
                    {
                        this.logger.error(error);
                        return error;
                    }                    
            }

        }

        this.logger.error(error);
            
        return Promise.reject(error.message || error);
    }

    public getValidator(): Promise<ValidatorField[]> {
        const url = `api/Validator/Product`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => {
                return response.json() as ValidatorField[];
            })
            .catch(this.handleError);
    }


    public getList(idProduct:number): Promise<any[]> {
        const url = `${this.serviceUrl}/${idProduct}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        return this.http.get(url, { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // public getList(idProduct:number): Promise<any[]> {
    //     return this.languageService.getCurrentLanguageId().then(idLanguage => 
    //             this.getListByIdLanguage(idLanguage, idProduct))
    // }

    // private getListByIdLanguage(idLanguage:number, idProduct:number): Promise<any[]> {
    //     const url = `${this.serviceUrl}/${idLanguage}/${idProduct}`;
    //     let authHeaders: Headers = this.authService.initAuthHeaders();
    //     return this.http.get(url, { headers: authHeaders })
    //         .toPromise()
    //         .then(response => response.json())
    //         .catch(this.handleError);
    // }

    public updateElements(list: ProductMaterial[]): Promise<any> {
        const url = `${this.serviceUrl}`;
        let authHeaders: Headers = this.authService.initAuthHeaders();
        authHeaders.append("Content-Type", "application/json");
        return this.http.put(url, JSON.stringify(list), { headers: authHeaders })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
}