import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  moduleId: module.id,
  selector: 'app-spinner',
  template: `<div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
    <div *ngIf="showMessage" style="text-align: center;">{{ 'Loading' | translate }}...</div>
    `
})
export class AppSpinnerComponent {
    @Input() showMessage: boolean = false;

    constructor(private translate: TranslateService) {
    }
}