import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { TranslateService } from '@ngx-translate/core';

export interface ConfirmModel {
  title:string;
  message:string;
}

@Component({  
    selector: 'app-confirm',
    template: `<div class="modal-dialog modal-primary">
                <div class="modal-content">
                   <div class="modal-header">
                    <h4 class="modal-title">{{title | translate}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                   <div class="modal-body">
                     <p>{{message | translate}}</p>
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-primary" (click)="confirm()">{{'_Ok' | translate}}</button>
                     <button type="button" class="btn btn-secondary" (click)="close()" >{{'_Cancel' | translate}}</button>
                   </div>
                 </div>
              </div>`
})
export class AppConfirmComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  
  public title: string;
  public message: string;

  constructor(dialogService: DialogService, 
    private translate: TranslateService) {
    super(dialogService);
  }

  confirm() {
    this.result = true;
    this.close();
  }

}