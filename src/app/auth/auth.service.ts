import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import { Router, ActivatedRoute } from "@angular/router";
import "rxjs/add/operator/toPromise";

import { NGXLogger } from 'ngx-logger';
import { User } from '../user/user'

import { ApiResponse, ValidatorField } from "../_shared";
import { AppService } from "../app.service";

@Injectable()
export class AuthService {
    public user: User;
    private token: string;

    private serviceUrl = 'api/Auth'; 
    private headers = new Headers({ 'Content-Type': 'application/json' });

    private isLoaded: boolean = false;

    constructor(private logger: NGXLogger,
        private http: Http ,
        private appService: AppService, 
        private router: Router,
        private route: ActivatedRoute) {

            this.logger.trace('AuthService Constructor');

            if (this.isLoaded)
                return;

            this.logger.trace('Loading user credentials from Storage..');
            this.user = this.appService.loadFromStorage("user", true);
            this.token = this.appService.loadFromStorage("token", false);

            if (this.user != null)
                this.logger.info('Retrieved User from Storage');
            if (this.token != null)
                this.logger.info('Retrieved Token from Storage');

            this.isLoaded = true;
    }
    
    private handleError(error: any): Promise<any> {
        this.logger.error(error);

        if (error.status == 401) {
            sessionStorage.setItem("token", null);
        }
        
        return Promise.reject(error.message || error);
    }

    public login(userName: string, password: string, userAgent: string): Promise<any> {
        let credentials = { Username: userName, Password: password };
        return this.http.post(this.serviceUrl, JSON.stringify(credentials), { headers: this.headers })
            .toPromise()
            .then(response => {
                this.logger.trace('AuthService login() response received.');
                let result = response.json();
                return result;
            })
            .catch(this.handleError);
    }

    public isLoggedIn() { 
        return this.token != null && this.user != null;
    }

    public getValidator(): Promise<ValidatorField[]> {
        const url = `api/Validator/Auth`;
        return this.http.get(url)
            .toPromise()
            .then(response => {
                this.logger.trace('AuthService getValidator() response received.');
                return response.json() as ValidatorField[];
            })
            .catch(this.handleError);
    }

    public getUserInfo(token?: string): Promise<User> {
        let authHeaders: Headers = this.initAuthHeaders(token);
        return this.http.get(this.serviceUrl, { headers: authHeaders }).toPromise()
            .then(response => {
                let user = response.json();
                this.sessionLogin(token, user)
                return user;
            })
            .catch(this.handleError);
    }
    
    private sessionLogin(token: string, user: User)
    {
        this.user = user;
        this.token = token;
        sessionStorage.setItem("user", JSON.stringify(user));
        sessionStorage.setItem("token", token);
    }
    
    initAuthHeaders(newToken?:string): Headers {
        let token: string = newToken;
        if (newToken == null)
            token = this.token;

        var headers = new Headers();
        headers.append("Authorization", "Bearer " + token);

        return headers;
    }

    getToken(): string
    {
        return this.token;
    }
    
    logOut()
    {
        this.logger.info('Logout.');
        this.token = null;
        this.user = null;
        sessionStorage.setItem("user", null);
        sessionStorage.setItem("token", null);
        //this.onLogOut.trigger();
    }

    public found401()
    {
        this.logger.trace('AuthService 401');
        this.router.navigateByUrl('/login');
        this.logOut();
    }
    //     private readonly onLogIn = new LiteEvent();
    //     private readonly onLogOut = new LiteEvent();
    
    //     public get loggedIn() { return this.onLogIn.expose(); }
    //     public get loggedOut() { return this.onLogOut.expose(); }
}