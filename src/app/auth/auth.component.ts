import { Component, OnInit, OnDestroy  } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';

import { NGXLogger } from 'ngx-logger';
import { Ng2DeviceService } from 'ng2-device-detector';
import { TranslateService } from '@ngx-translate/core';

import { AppService } from '../app.service';
import { ApiResponse, AppToastrService, ValidatorField, ValidatorEngine } from '../_shared';

import { UserCredentials } from './user-credentials';
import { User } from '../user/user';

import { AuthService } from './auth.service';

@Component({
  moduleId: module.id,
  selector: 'agro-auth',
  templateUrl: 'auth.component.html'
})
export class AuthComponent implements OnDestroy {

    private credentials: UserCredentials;
    private sub;
  
    private validator: ValidatorEngine;
    public loginForm: FormGroup;

    private user: User;
    private deviceInfo: any;
    private promiseLogin;
    public connectionError: boolean = false;

    version = {Front : "0.1.0.0", Back: "-", DB : "-"};
  
    constructor(
        private logger: NGXLogger,
        private appServices: AppService,
        private authService: AuthService,
        private fb: FormBuilder,
        private deviceService: Ng2DeviceService,
        public translate: TranslateService,
        public toastr: AppToastrService, 
        private router: Router) {

        this.deviceInfo = this.deviceService.getDeviceInfo();
        this.logger.info('Device Info: ' + JSON.stringify(this.deviceInfo));

        translate.addLangs(["es", "en", "ru"]);
        translate.setDefaultLang('es');

        let browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es/) ? browserLang : 'es');
        this.sub = translate.onLangChange.subscribe((event) => {
            this.validator.buildMessages(translate);
        })

        this.credentials = { Username: '', Password: '' };

        this.validator = new ValidatorEngine();

        this.validator.Builded.on(this.formBuilded);
    }


    ngOnInit(): void {
        this.getValidatorDefinition();
        this.connectionError = false;
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
      }

    getValidatorDefinition(): void {
        this.authService.getValidator()
            .then(d => {
                this.validator.BuildForm(d, this.translate);
            })
            .catch(err => this.connectionError = true);
    }


    formBuilded = () => {
        this.loginForm = this.validator.Form;
    }

    onLogin() {
        this.toastr.clear();
        if (this.loginForm.invalid)
        {
            this.validator.invalidSubmit();
            this.toastr.errorTranslate("_ReviewInvalidFileds");
            return;
        }

        this.credentials = this.loginForm.value;
        this.promiseLogin = this.authService.login(this.credentials.Username, this.credentials.Password, 
            this.deviceInfo.userAgent)
            .then(ar => {
                this.logger.debug('Login Ok. Requesting User...');
                this.getUser(ar.accessToken);
            })
            .catch(err => {
                this.toastr.errorTranslate("Auth_LoginFailed");
            });
    }

    getUser(token:string) {
        this.promiseLogin = this.authService.getUserInfo(token)
            .then(ar => {
                this.logger.debug('Response User. Redirecting...');
                this.router.navigate(['/admin/dashboard']);
            });
    }
}
