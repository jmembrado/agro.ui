# Angular2DevelopmentCLI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-beta.32.3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

##External



    - Json2Typescript: https://github.com/dhlab-basel/json2typescript
    - Template: http://coreui.io/docs/getting-started/introduction/ 
    - Devide Info: https://www.npmjs.com/package/ng2-device-detector
    - Logger: https://www.npmjs.com/package/ngx-logger
    - Traducciones: http://www.ngx-translate.com/ https://www.npmjs.com/package/ng2-translate 
    - Mensajes: https://github.com/scttcper/ngx-toastr
    - Modal: https://www.npmjs.com/package/ng2-bootstrap-modal
    - Promis button: https://github.com/johannesjo/angular2-promise-buttons
    - Datatable: https://swimlane.github.io/ngx-datatable/
    - Select: https://basvandenberg.github.io/ng-select#/home
    - BreadCrumbs: https://github.com/gmostert/ng2-breadcrumb
    - Tree: https://github.com/500tech/angular-tree-component
    - FileUpload: https://github.com/bleenco/ngx-uploader
    - StarRating: https://github.com/BioPhoton/angular-star-rating

##Pendint
    - Ng-Select: AddressDetails, EntityDetailsDefinition: Traducciones.
    - AddressList: Busqueda por Pais