﻿
var events = {
  "tabCreated": chrome.tabs.onCreated,
  "tabMoved": chrome.tabs.onMoved,
  "tabRemoved": chrome.tabs.onRemoved,
  "tabSelectionChanged": chrome.tabs.onSelectionChanged,
  "tabAttached": chrome.tabs.onAttached,
  "tabDetached": chrome.tabs.onDetached,
  "tabNavigated": chrome.tabs.onUpdated,
  "windowCreated": chrome.windows.onCreated,
  "windowFocusChanged": chrome.windows.onFocusChanged,
  "bookmarkCreated": chrome.bookmarks.onCreated,
  "bookmarkMoved": chrome.bookmarks.onMoved,
  "bookmarkRemoved": chrome.bookmarks.onRemoved
};

// Map of event name to a validation function that is should return true if
// the default sound should be played for this event.
var eventValidator = {
  "tabNavigated":tabNavigated
};
var currentTabID = -1;
var plugin = document.getElementById("pluginIdTFPU");
 //console.log('plugin.initMember'); 
 var result = plugin.InitMember();
  if(false != result)
  {
     plugin.ConstructConnect("StartAnalyse\0", "ContinueAnalyse\0","InitFillDataArray\0", "FillDataToForm\0", "WriteData\0");
	 //console.log("plugin.ConstructConnect");
  }
function soundEvent(event, name) {

	
  if (event) {
    var validator = eventValidator[name];
    if (validator) {
      event.addListener(function() {
        //console.log("handling custom event: " + name);

        if (validator.apply(this, arguments)) {


        }
      });
    } else {
      event.addListener(function() {
        //console.log("handling event: " + name);
        if (eatEvent(name)) {
          return;
        }

      });
    }
  } else {
    //console.log("no event for " + name);
  }
}

function tabNavigated(tabId, changeInfo, tab)
{
		
    var bRet = false;
	//console.log("tabNavigated started");
     try
    {  
     if("complete"==tab.status)
     {
         //console.log("tabNavigated " + frames);
    	 //console.log("tabNavigated " + frames.length);   
      	 var bIsPWDValid = plugin.IsPWDValid();
      	 if (bIsPWDValid) 
         {
             var strUrl = tab.url + "\0";
             var bIsUrlRegisterd = plugin.IsUrlRegistered(strUrl);
		     var strColor = "";
			 var iLangID = plugin.getLangID();
		     if (bIsUrlRegisterd)
		     {
				 strColor = plugin.getRegisterColor(true);
		     }
		     else
			 {
                  strColor = plugin.getRegisterColor(false);
			 }
			 //console.log("tabNavigated " + strColor);
      	     bRet = ShowTFPUMessag(tab.id , bIsUrlRegisterd, strColor, iLangID);
 	     }
		 else
		 {
		     //console.log("tabNavigated bIsPWDValid");   
		 }
     }
    }
    catch(ex)
    {
        //console.log("window.onload: init1 failed");
        //console.log(ex);
    }   

}

 function ShowTFPUMessag(tabId, bIsUrlRegisterd, strColor, iLangID)
 {
      var bRet = false;
     //console.log( "ShowTFPUMessag IsPasswordExist");
      var MessageString = {FunctionName:"ShowTFPUMessag", IsUrlRegistered:bIsUrlRegisterd, strRegColore:strColor, iLanguageID:iLangID};
	
	  //console.log("ShowTFPUMessag message string");


      chrome.tabs.sendRequest(tabId, MessageString, function(bIsExist) 
      {
            bRet = bIsExist;
			//console.log("sendRequest");

       });
       return bRet;
 }

function StartAnalyse()
{
    //console.log('startAnalyse= bg.js ' );
    var MessageString = {FunctionName:"IsPasswordExist"};  
	//alert("IsPasswordExist");
    chrome.tabs.getSelected(null, function(tab)
     {
       var strUrl = tab.url + "\0";
       currentTabID = tab.id;
       var result = plugin.SetCurrentUrl(strUrl); 
	   //console.log('StartAnalyse plugin.SetCurrentUrl:' + strUrl + '  result:' + result );
       SendRequestToContent(MessageString);   
    });
  
  return true;  
}

function InitFillDataArray(nCount)
{
      var bRet = true;
      var MessageString = {FunctionName:"InitFillDataArray", nArrayCount:nCount};    
      SendRequestToContent(MessageString);
       return bRet;
}



function ContinueAnalyse(nFlag, nCount)
{    
      var bRet = true;
      var MessageString = {FunctionName:"continueAnalyse", Flag:nFlag , Count:nCount};
      SendRequestToContent(MessageString);
      return bRet;
}

function FillDataToForm(strIndex, strID, strName, strType, strText)
{ 
      //console.log("FillDataToForm " + strIndex + strID+ strName+  strType+ "  Text:" + strText);
      var bRet = true;
      var MessageString = {FunctionName:"FillDataToForm",Index:strIndex, ID:strID , Name:strName, Type:strType, Text:strText};    
      SendRequestToContent(MessageString);
      
     return bRet;   
}

function WriteData(strFormId, strFormName, strFormIndex)
{
     //console.log("WriteData");
     var bRet = true;
     var MessageString = {FunctionName:"WriteData",FormId:strFormId, FormName:strFormName , FormIndex:strFormIndex};    
     SendRequestToContent(MessageString);
}

function SendRequestToContent(MessageString)
{
	 chrome.tabs.sendRequest(currentTabID, MessageString, function(Data) 
      {
      
          if(Data.FunctionName == "GetPosswordFailed")
          {
               GetPosswordFailed(Data.Type);
          }
          else if(Data.FunctionName == "TransferData")
          {
              var dataArray = Data.datas;
              var strWebName = Data.WebName + "\0";
              var strFormID = Data.FormID + "\0";
              var strFormName = Data.FormName + "\0";                            
            //console.log(dataArray+Data.strFormID);
              TransferData(dataArray);
              plugin.Save(strWebName, strFormID, strFormName, Data.ReadIndex);           
          }
          else if(Data.FunctionName == "IsEmptyReturn")
          {
              if(Data.isEmpty == false)
              {
                  var dataArray = Data.datas;
                  var strFormID = Data.FormID + "\0";
                  var strFormName = Data.FormName + "\0"; 
                  TransferData(dataArray);
                  plugin.TransferSubData(strFormID, strFormName);         
              }
              plugin.ContinueAnalyzeReturn(Data.isEmpty);              
          }
          else if(Data.FunctionName == "GetPosswordReturn")
          {
              plugin.SetIsPasswordExist(Data.PWDExist);              
          }
      });
}

chrome.extension.onRequest.addListener(function(req, sender, sendResponse)
{
	if (req.FunctionName = "openHelpFile")
	{
		plugin.openHelpFile();
	}
});

function GetPosswordFailed(nType)
{
    plugin.GetPosswordFailed(nType);
	//console.log("plugin.GetPosswordFailed" + nType);
}

function TransferData(dataArray)
{
     try
    {
    	for(var nIndex = 0; nIndex < dataArray.length; nIndex++)
    	{
    	    if (dataArray[nIndex].length == 4) 
       		{
       		    var strID = dataArray[nIndex][0] + "\0";
       		    var strName = dataArray[nIndex][1] + "\0";
       		    var strType = dataArray[nIndex][2] + "\0";  
       		    var strText = dataArray[nIndex][3] + "\0";
       		    plugin.TransferData(strID, strName, strType, strText );
     	  	}

    	}
    }  
    catch(ex)
    {
        //console.log("TransferData" + ex);
    }    
}
      
for (var name in events)
{
  soundEvent(events[name], name);
}