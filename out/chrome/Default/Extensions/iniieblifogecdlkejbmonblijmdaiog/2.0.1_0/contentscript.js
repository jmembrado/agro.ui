  //  alert("contentscript"); 
 
  //chrome.extension.sendRequest({message1: "setSession1", message2: "setSession2"}, function(response) {}); 
var   g_strFontColor       = "#004000";
var   g_strBorderColor     = "#008000";
var   g_strDivID           = "TFPUPWDBankNotify";
var   g_strBtnHelpID       = "TFPUPWDBankHelpButton";
var   g_strBtnHelpCaption  = "Help";
var   g_strBtnCloseCaption = "Close";
var   g_strRegBgColor      = "#00FF00";
var   g_strNotRegBgColor   = "#FFFF00";
var   g_strRegMsg          = "This website has been registered for automatic password input, please swipe your finger!";
var   g_strNotRegMsg       = "This website has not been registered for automatic password input yet, please swipe your finger!";
var   g_bRegistered        = false;
var   g_textFirst          = true;
var   g_suitFormArray      = null;
var   g_dataArray          = null;
var   g_readIndex          = 0;
var   g_strBGColor         = "";
var g_iLangID = 0;


chrome.extension.onRequest.addListener(function(req, sender, sendResponse)
{
   var bRet = false;
   if(g_suitFormArray == null)
   {
       g_suitFormArray = new Array();     
   }
   //console.log("FillDataToForm");
    var bPWDExist =  FxDocumentAnalyze.IsPasswordExist();// TODO do not need do it every time
    if(bPWDExist)
    {
    	//console.log(req.message3);
    	//console.log(req.FunctionName); 
   	 if(req.FunctionName == "continueAnalyse")
    	{
        	continueAnalyse(req.Flag, req.Count , sendResponse);
    	}
    	if(req.FunctionName == "FillDataToForm")
    	{
			 //console.log("FillDataToForm in contentscript: " + req.Index + req.ID+  req.Name+  req.Type+ "  Text:" +  req.Text);
        	FillDataToForm(req.Index, req.ID , req.Name, req.Type, req.Text, sendResponse);
    	}
    	else if (req.FunctionName == "WriteData")
    	{
        	WriteData(req.FormId, req.FormName , req.FormIndex);
    
    	}
    	else if(req.FunctionName == "ShowTFPUMessag")
    	{  
			//console.log("ShowTFPUMessag don't show  password message "); 
	 
   	 	    if (bPWDExist)
  	  	    {
  	       		g_bRegistered  = req.IsUrlRegistered;
				g_strBGColor = req.strRegColore;
				g_iLangID = req.iLanguageID;
 	       		InsertAdjacentHTML();
   	 	    } 
   		    else
   	 	    {
   	       	 //console.log("don't show  password message "); 
   	 	    }
        	bRet = true; 
    	}
    	else if(req.FunctionName == "InitFillDataArray")
    	{
              InitFillDataArray(req.nArrayCount)
    	}
    	else if(req.FunctionName == "IsPasswordExist")
    	{
              IsPasswordExist(sendResponse);
    	}
    }
    sendResponse(bRet,1);
});

function IsPasswordExist(callBackFunction)
{
      var bPWDExist =  FxDocumentAnalyze.IsPasswordExist();
      var MessageString = {FunctionName:"GetPosswordReturn", PWDExist:bPWDExist};
      callBackFunction(MessageString);          
}

function WriteData(strFormId, strFormName , strFormIndex)
{
    //console.log("WriteData  start" );
    try
    {
        FxDocumentAnalyze.WriteDataToForm(strFormId, strFormName, strFormIndex, g_dataArray);
    }
    catch(ex)
    { 
        //console.log("writeData failed");
        //console.log(ex);
    }         
}

function InitFillDataArray(nCount)
{
    if(nCount > 0)
    {   
         g_dataArray = new Array();
         for(var nIndex = 0; nIndex < nCount; nIndex++)
          {
                g_dataArray[nIndex]  = new Array(4);
          }
    }
}

function FillDataToForm(strIndex,strID , strName, strType, strText, callBackFunction)
{

    try
    {
     g_dataArray[strIndex][0] = strID.toString();
   
     g_dataArray[strIndex][1] = strName;
      
     g_dataArray[strIndex][2] = strType;
 
     g_dataArray[strIndex][3] = strText;    
    }
    catch(ex)
    {
        //console.log("FillDataToForm failed ++");
        //console.log(ex);
    }                
    //console.log("FillDataToForm  ContentScript" + g_dataArray[0][3]);    
}

function continueAnalyse(nFlag, nCount, callBackFunction)
{
      var bPWDExist =  FxDocumentAnalyze.IsPasswordExist();

     try
    {
        if(1 == nFlag) // read
        {

            var readIndex;
            var formEle = FxDocumentAnalyze.ConfirmForm(readIndex, -1, true);
     
            if(null == formEle)
            {
                  var MessageString = {FunctionName:"GetPosswordFailed", Type:1};
                callBackFunction(MessageString);
                //TFPUCom.fail(1);
            }
            else if(FxDocumentAnalyze.IsFormEmpty(formEle))
            {
                  var MessageString = {FunctionName:"GetPosswordFailed", Type:0};
                  callBackFunction(MessageString);            
               // TFPUCom.fail(0);
            }
            else
            {
                var dataArray = FxDocumentAnalyze.GetFormData(formEle);
                if(null == dataArray || 0 == dataArray.length)
                {
                        //console.log('dataArray NULL');       
                    return;
                }

                //console.log(dataArray);

                var strWebName = FxDocumentAnalyze.GetFirstText(formEle);
                 var strFormID = "";
        
                var strFormName = formEle.name;
                var strReadIndex = g_readIndex;
                if(typeof(formEle.id)!='undefined')
                {
                   strFormID = formEle.id;
                }
                var MessageString = {FunctionName:"TransferData", datas:dataArray, WebName:strWebName, FormID:strFormID, FormName:strFormName, ReadIndex:strReadIndex};
                //var MessageString = {FunctionName:"TransferData", FormID:strFormID};
                
                callBackFunction(MessageString);               
               // TFPUCom.save(FxDocumentAnalyze.GetCurrentActiveUrl(), FxDocumentAnalyze.GetFirstText(formEle), formEle.id, formEle.name, g_readIndex);
            }        
        }
        else if(0 == nFlag) // write
        {   
			//console.log(nCount);
        }
        else if(2 == nFlag) //isEmpty
        {

            var actForm = FxDocumentAnalyze.ConfirmForm(readIndex, -1, true);
            var bIsPasswordEmpty = false;
            if(null == actForm)
            {
                //console.log("actForm null");
                bIsPasswordEmpty = true;
            }
            else if(!FxDocumentAnalyze.IsPasswordExistInForm(actForm))
            {
                //console.log("no IsPasswordExistInForm");            
                bIsPasswordEmpty = true;
            }      
            else if(FxDocumentAnalyze.IsFormEmpty(actForm))
            {
                //console.log("FxDocumentAnalyze IsFormEmpty");               
                bIsPasswordEmpty =  true;
            }
            var MessageString;
            if(bIsPasswordEmpty == false)
            {
               //var actForm = FxDocumentAnalyze.GetActiveForm();
               //console.log(actForm);
               var dataArray = FxDocumentAnalyze.GetSubFormData(actForm);
               var strFormID = "";
               var strFormName = actForm.name;
               if(typeof(actForm.id)!='undefined')
               {
                  //console.log(typeof(actForm.id));
                   strFormID = actForm.id;
               }
               MessageString = {FunctionName:"IsEmptyReturn", isEmpty:bIsPasswordEmpty, datas:dataArray, FormID:strFormID, FormName:strFormName};
            }
            else
            {
                MessageString = {FunctionName:"IsEmptyReturn", isEmpty:bIsPasswordEmpty};          
            }
            callBackFunction(MessageString);                 

        }
       
        return true;
    }
    catch(ex)
    {
        //console.log("continueAnalyse failed");
        //console.log(ex);
    }        
    //console.log("continueAnalyse(nFlag, nCount)"); 
}


function InsertAdjacentHTML()
{
    try
    {
        var curDoc   = document;
        var body_tag = document.body;

        if(curDoc.getElementById(g_strBtnHelpID) != null)
        {
            return;
        }
        // get langID from XPCOM component
        var langID = 1028; //1033; 
        
        langID = g_iLangID;
       
        CommonModule.InitStringByLangID(langID);
        
        var htmlStr = CommonModule.GetDivHtml();
       
        var r = body_tag.ownerDocument.createRange();   

        r.setStartBefore(body_tag);
        var parsedHTML = r.createContextualFragment(htmlStr);
        if(null == parsedHTML)
        {
            return;
        }      
        body_tag.insertBefore(parsedHTML, body_tag.firstChild);

        var btnHelp = curDoc.getElementById(g_strBtnHelpID);
	    if(null != btnHelp)
	    {
	        btnHelp.addEventListener("click", CommonModule.OpenTFPUHelpFile, true);
	    }	
    }
    catch(ex)
    {
        //console.log("InsertAdjacentHTML");
        //console.log(ex);
    }
}



var FxDocumentAnalyze = 
{
    GetCurrentActiveDocument : function()
    {
        try
        {
			/* WGY:
            var gb = window.gBrowser;
            
            var active_window = gb.getBrowserAtIndex(gb.mTabContainer.selectedIndex).contentWindow;

            // Finally, the document.
            var active_document = active_window.content.document;
            
            return active_document;
			//*/
			return document;
        }
        catch(ex)
        {
            //console.log("GetCurrentActiveDocument");
            //console.log(ex);
        }
    },
    
    GetBodyOfCurDoc : function()
    {
        try
        {
            var active_document = this.GetCurrentActiveDocument(); 
            
            var body_tags = active_document.getElementsByTagName("body");
            
            if(body_tags.length <= 0) 
            {
                return;
            }
            
            var body_tag = body_tags.item(0);
            
            return body_tag;
        }
        catch(ex)
        {
            //console.log("GetBodyOfCurDoc");
            //console.log(ex);
        }
    },
    
    GetCurrentActiveUrl : function()
    {
        var urlValue = window.content.document.location.href;        
	
	    if(urlValue != null)
	    {
	        
		    if(urlValue.toLowerCase().indexOf("login.live.com")>-1)
		    {
			    urlValue="login.live.com";
		    }
	    }
	    else
	    {
	        return "";
	    }
    	
	    return urlValue;        
    },

    GetActiveElement : function()
    {
        //console.log(document);
        //console.log(document.commandDispatcher);      
        return document.commandDispatcher.focusedElement;
    },
    
    GetFrames : function(frame, documentList)
    {
        try
        {
            //var framesList = frame.frames;
            var framesList = frame.getElementsByTagName('FRAME');
            var iframeList = frame.getElementsByTagName('iframe');
            documentList.push(frame);
            
            // Loop through the frames
            for(var i = 0; i < framesList.length; i++)
            {
                this.GetFrames(framesList[i].contentDocument, documentList);
            }
            for(var i = 0; i < iframeList.length; i++)
            {
                this.GetFrames(iframeList[i].contentDocument, documentList);
            }
            return documentList;
        }
        catch(ex)
        {
            //console.log("GetFrames failed");
            //console.log(ex);
        }
    },
    
    GetActiveForm : function()
    {
        try
        {
          var localframes = this.GetFrames(window.content, new Array())  
          var formArray;
          
          for(var j = 0; j < localframes.length; j++)
          { 
             formArray = localframes[j].forms;    
             
             var formelements;
             for(var l = 0 ; l < formArray.length ; l++)
             {
                 if(this.IsPasswordExistInForm(formArray[l]))
                 {
					 return formArray[l];
                 }
              }
            }    
            return null;
		}
        catch(ex)
        {
            ////console.log("GetActiveForm");
            ////console.log(ex);
        }
    },
    
    IsPasswordExistInForm : function (vform)
    {
        try
        {
            if(null == vform)
            {
                return false;
            }
            
	        var passwordCount = 0;
	        var textCount = 0;
	        var inputArray = vform.getElementsByTagName("input")
        	var elementForm= vform.elements;
        	
	        if(inputArray.length != 0)
	        {    
		        for(var k = 0; k < inputArray.length; k++)
		        {
			        if (inputArray[k].type == "password")
			        {
				        passwordCount++;
			        }
			        else if(inputArray[k].type == "text")
			        {
			            textCount++;
			        }
		        }
	        }
        	else
            {     
                for(var i = 0; i < elementForm.length; i++)
                {
                    if(elementForm[i].type == "password")
                    {
						passwordCount++;
					}
					else if(elementForm[i].type == "text")
					{
					    textCount++;
					}
                }          
            }
 	        if(1 == passwordCount && textCount > 0)
	        {
	            g_suitFormArray.push(vform);
  	            return true;
	        }
        	
	        return false;
        }
        catch(ex)
        {
            //console.log("IsPasswordExistInForm failed");
            //console.log(ex);
        }
    },
    
    IsPasswordExist : function()
    {
      var isfound = false;
          
      try
      {
         //console.log("IsPasswordExist");
         
          g_suitFormArray.splice(0, g_suitFormArray.length);
          
          var isfound = false;

          var localframes = this.GetFrames(document, new Array())  
          var formArray;
         
          for(var j = 0; j < localframes.length; j++)
          { 
            formArray = localframes[j].forms;    
           // formArray = document.forms;    
            var formelements;
            for(var l = 0 ; l < formArray.length ; l++)
            {
                if(this.IsPasswordExistInForm(formArray[l]))
                {
                    //console.log("find password!");
                    isfound  = true;
                   //console.log("!!! WGY Found password in form:" + formArray[l].name);
                }
            }
          }    
            
          delete  localframes; 

          return isfound;
      }
      catch(ex)
      {
          //console.log("IsPasswordExist");
          //console.log(ex);
      }
    },

    
    IsFormEmpty : function (formEle)
    {
        try
        {
            if(formEle == null)
            {
                return true;
            }
            
            var isEmpty = false;
            var inputValue = null;
            
            var inputArray = formEle.getElementsByTagName("input");
            var elemForm   = formEle.elements;
            
            if(inputArray.length != 0)
            {
                for(var nIndex = 0; nIndex < inputArray.length; nIndex++)
                {
                    if((inputArray[nIndex].type == "text") || (inputArray[nIndex].type == "password"))
                    {
                        if(inputArray[nIndex].value == "")
                        {
                            isEmpty = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                for(var nIndex = 0; nIndex < elemForm.length; nIndex++)
                {
                    if((elemForm[nIndex].type == "text") || (elemForm[nIndex].type == "password"))
                    {
                        if(elemForm[nIndex].value == "")
                        {
                            isEmpty = true;
                            break;
                        }
                    }
                }
            }
            
            return isEmpty;
        }
        catch(ex)
        {
            //console.log("isEmpty");
            //console.log(ex);
        }
    },
        
    GetFirstText : function (formEle)
    {
        try
        {
            if(null == formEle)
            {
                return "";
            }
            
            var inputArray = formEle.getElementsByTagName("input");
            var elemArray  = formEle.elements;
            
            if(inputArray.length != 0)
            {
                for(var nIndex = 0; nIndex < inputArray.length; nIndex++)
                {
                    if(inputArray[nIndex].type == "text")
                    {
                        return inputArray[nIndex].value;
                    }
                    
                }
            }
            else
            {
                for(var nIndex = 0; nIndex < elemArray.length; nIndex++)
                {
                    if(elemArray[nIndex].type == "text")
                    {
                        return elemArray[nIndex].value;
                    }
                }
            }
            
            return "";
        }
        catch(ex)
        {
            //console.log("GetFirstText");
            //console.log(ex);
        }
    },
    
    GetFormData : function(formEle)
    {    
        try
        {
            if(null == formEle)
            {
                return;
            }
            
            var formData = new Array();
            var nCountSave = 0;
            
            var selectArray = formEle.getElementsByTagName("select");
            var nSelCount = selectArray.length;
            
            nCountSave = nCountSave + nSelCount;
            
            var inputArray = formEle.getElementsByTagName("input");
            var elemForm   = formEle.elements;
            
            if(inputArray.length != 0 )
            {
                for(var nIndex = 0; nIndex < inputArray.length; nIndex++)
                {
                    if(!((inputArray[nIndex].type == "text") || (inputArray[nIndex].type == "password")
                      || (inputArray[nIndex].type == "checkbox") || (inputArray[nIndex].type == "radio")))
                    {
                        continue;
                    }
                    nCountSave = nCountSave + 1;
                }
            }
            else
            {
                for(var nIndex = 0; nIndex < elemForm.length; nIndex++)
                {
                    if(!((elemForm[nIndex].type == "text") || (elemForm[nIndex].type == "password")
                      || (elemForm[nIndex].type == "checkbox") || (elemForm[nIndex].type == "radio")))
                    {
                        continue;
                    }
                    nCountSave = nCountSave + 1;
                }
            }
            
            for(var nIndex = 0; nIndex < nCountSave; nIndex++)
            {
                formData[nIndex] = new Array(4);
            }
            
            var selIndex = 0;    
            for(var nIndex = 0; nIndex < nSelCount; nIndex++)
            {
                formData[nIndex][0] = selectArray[nIndex].id;
                formData[nIndex][1] = selectArray[nIndex].name;
                formData[nIndex][2] = selectArray[nIndex].type;
               
                selIndex = selectArray[nIndex].selectedIndex;
                formData[nIndex][3] = selectArray[nIndex].options[selIndex].text;
            }
            
            var curIndex = nSelCount;
            
            if(inputArray.length != 0)
            {
                for(var nIndex = 0; nIndex < inputArray.length; nIndex++)
                {
                    if(!((inputArray[nIndex].type == "text") || (inputArray[nIndex].type == "password")
                      || (inputArray[nIndex].type == "checkbox") || (inputArray[nIndex].type == "radio")))
                    {
                        continue;
                    }
                    
                    formData[curIndex][0] = inputArray[nIndex].id;
                    formData[curIndex][1] = inputArray[nIndex].name;
                    formData[curIndex][2] = inputArray[nIndex].type;
                    if(inputArray[nIndex].type != "checkbox")
                    {
                        formData[curIndex][3] = inputArray[nIndex].value;
                    }
                    else
                    {
                        if(inputArray[nIndex].checked)
                        {
                            formData[curIndex][3] = "checked";
                        }
                        else
                        {
                            formData[curIndex][3] = "unchecked";
                        }
                    }
                    curIndex++;
                }
            }
            else
            {
                for(var nIndex = 0; nIndex < elemForm.length; nIndex++)
                {
                    if(!((elemForm[nIndex].type == "text") || (elemForm[nIndex].type == "password")
                      || (elemForm[nIndex].type == "checkbox") || (elemForm[nIndex].type == "radio")))
                    {
                        continue;
                    }
                    
                    formData[curIndex][0] = elemForm[nIndex].id;
                    formData[curIndex][1] = elemForm[nIndex].name;
                    formData[curIndex][2] = elemForm[nIndex].type;
                    if(elemForm[nIndex].type != "checkbox")
                    {
                        formData[curIndex][3] = elemForm[nIndex].value;
                    }
                    else
                    {
                        if(elemForm[nIndex].checked)
                        {
                            formData[curIndex][3] = "checked";
                        }
                        else
                        {
                            formData[curIndex][3] = "unchecked";
                        }
                    }
                    curIndex++;
                }
            }
            
            // delete formData; // delete the data out of the function
            
            return formData;
        }
        catch(ex)
        {
            //console.log("getformData");
            //console.log(ex);
        }
    },
    
    GetSubFormData : function (formEle)
    {
        try
        {
            if(formEle == null)
            {
                return null;
            }
            
            var formData = new Array();
             for(var nIndex = 0; nIndex < 2; nIndex++)
            {
                formData[nIndex] = new Array(4);
            }    
            
            var inputArray = formEle.getElementsByTagName("input");
            var elemForm   = formEle.elements;
            
            var formIndex = 0;
            var textCount = 0;
            if(inputArray.length != 0)
            {
                for(var nIndex = 0; nIndex < inputArray.length; nIndex++)
                {
                    if(!((inputArray[nIndex].type == "text") || (inputArray[nIndex].type == "password")))
                    {
                        continue;
                    }
                    
                    if(inputArray[nIndex].type == "text")
                    {   
                        textCount++;  
                        if(textCount > 1)    
                        {
                            continue;
                        }      
                    }
                    else 
                    {
                        if(textCount > 0)
                        {
                            g_textFirst = true;
                        }
                        else
                        {
                            g_textFirst = false;
                        }
                    }
                    
                    formData[formIndex][0] = inputArray[nIndex].id;
                    formData[formIndex][1] = inputArray[nIndex].name;
                    formData[formIndex][2] = inputArray[nIndex].type;
                    formData[formIndex][3] = inputArray[nIndex].value;
                    
                    formIndex++;
                }
            }
            else
            {
                for(var nIndex = 0; nIndex < elemForm.length; nIndex++)
                {
                    if(!((elemForm[nIndex].type == "text") || (elemForm[nIndex].type == "password")))
                    {
                        continue;
                    }
                    
                    if(elemForm[nIndex].type == "text")
                    {   
                        textCount++;  
                        if(textCount > 1)    
                        {
                            continue;
                        }      
                    }
                    else 
                    {
                        if(textCount > 0)
                        {
                            g_textFirst = true;
                        }
                        else
                        {
                            g_textFirst = false;
                        }
                    }
                    
                    formData[formIndex][0] = elemForm[nIndex].id;
                    formData[formIndex][1] = elemForm[nIndex].name;
                    formData[formIndex][2] = elemForm[nIndex].type;
                    formData[formIndex][3] = elemForm[nIndex].value;
                    
                    formIndex++;
                }
            }
            
            // delete formData; // delete the data out of the function
            
            return formData;
        }
        catch(ex)
        {
            //console.log("getsubformData");
            //console.log(ex);
        }
    },
    
    ConfirmForm : function(nFormReadIndex, nFormWriteIndex, bRead)
    {
        var nSize = g_suitFormArray.length;
	    	//console.log("DEBUG:::nSize=" + nSize);

	    if(0 == nSize)
	    {
		    return null;
	    }	
    	
	    nFormReadIndex = -1;

	    if(!bRead)
	    {
		    if(-1 != nFormWriteIndex && nFormWriteIndex < nSize)
		    {
			    return g_suitFormArray[nFormWriteIndex];
		    }
	    }
	    else
	    {	
		    if(1 == nSize)
		    {
			    g_readIndex = 0;
			    return g_suitFormArray[0];
		    }
		    else
		    {   
		         var activeForm = this.GetActiveForm();
		         if(null == activeForm)  
		         {
		            return null;
		         }
		         else
		         {
		            for(var nIndex = 0; nIndex < g_suitFormArray.length; nIndex++)
		            {
		                if(activeForm == g_suitFormArray[nIndex])
		                {
		                    g_readIndex = nIndex;
		                    return g_suitFormArray[nIndex];
		                }
		            }
		         }
		    }
	    }

	    return null;
    },

    GetBtnSubmit : function(formEle)
    {
        try
        {
            if(null == formEle)
            {
                return null;
            }
            
            var btnSubmit = null;
            var eleArray = formEle.elements;
            
            for(var nIndex = 0; nIndex < eleArray.length; nIndex++)
	        {
		        if(eleArray[nIndex].type == "submit")
		        {
			        btnSubmit = eleArray[nIndex];
			        break;
		        }
	        }
        	
	        if(btnSubmit == null)
	        {
	            var inputArray = formEle.getElementsByTagName("input");
                for(var nIndex = 0; nIndex < inputArray.length; nIndex++)
                {
                    if(inputArray[nIndex].type == "submit")
                    {
                        btnSubmit =  inputArray[nIndex];
                        break;
                    }
                }
	        }
            
            if(btnSubmit == null)
            {
                var btnArray = formEle.getElementsByTagName("button");
                for(var nIndex = 0; nIndex < btnArray.length; nIndex++)
                {
                    if(btnArray[nIndex].type == "submit")
                    {
                        btnSubmit =  btnArray[nIndex];
                        break;
                    }
                }
            }
            
            return btnSubmit;
        }
        catch(ex)
        {
            //console.log("GetbtnSubmit");
            //console.log(ex);
        }
    },
    
    AutoSubmitForm : function(formEle)
    {
        try
        {
            if(formEle == null)
            {
                return;
            }
            
            var btnSubmit = this.GetBtnSubmit(formEle);    
           
            if(btnSubmit != null)
            {       
                btnSubmit.click();
            }
            else
            { 
                formEle.submit();
            }
        }
        catch(ex)
        {
            //console.log("autoEmpty");
            //console.log(ex);
        }
    },

    WriteDataToForm : function(formId, formName, formIndex, dataArray)
    {
    //console.log("WriteDataToForm start");
        try
        {
            var formWrite = this.ConfirmForm(-1, formIndex, false);
            
            if(null == formWrite)
            {//console.log("ConfirmForm failed");
                return false;
            }
            //console.log("ConfirmForm succeed");
            var strId, strName, strType;
            
            var selectArray = formWrite.getElementsByTagName("select");
            for(var nIndex = 0; nIndex < selectArray.length; nIndex++)
            {
                strId = selectArray[nIndex].id;
                strName = selectArray[nIndex].name;
                strType = selectArray[nIndex].type;
                
                for(var nIndex1 = 0; nIndex1 < dataArray.length; nIndex1++)
                {
                    if(  strType == dataArray[nIndex1][2] 
                      && strId == dataArray[nIndex1][0] 
                      && strName == dataArray[nIndex1][1])
                    {
                        selectArray[nIndex].value = dataArray[nIndex1][3];
                        break;
                    }
                }
            }
            
            var inputArray = formWrite.getElementsByTagName("input");
            var elemArray  = formWrite.elements;
            
            if(inputArray.length != 0)
            {
                for(var nIndex = 0; nIndex < inputArray.length; nIndex++)
                {
                    strId = inputArray[nIndex].id;
                    strName = inputArray[nIndex].name;
                    strType = inputArray[nIndex].type;
                    
                    for(var nIndex1 = 0; nIndex1 < dataArray.length; nIndex1++)
                    {
                        if(  strType == dataArray[nIndex1][2] 
                          && strId == dataArray[nIndex1][0] 
                          && strName == dataArray[nIndex1][1])
                        {
                            inputArray[nIndex].value = dataArray[nIndex1][3];
                            break;
                        }
                    }
                }
            }
            else
            {
                for(var nIndex = 0; nIndex < elemArray.length; nIndex++)
                {
                    strId = elemArray[nIndex].id;
                    strName = elemArray[nIndex].name;
                    strType = elemArray[nIndex].type;
                    
                    for(var nIndex1 = 0; nIndex1 < dataArray.length; nIndex1++)
                    {
                        if(  strType == dataArray[nIndex1][2] 
                          && strId == dataArray[nIndex1][0] 
                          && strName == dataArray[nIndex1][1])
                        {
                            elemArray[nIndex].value = dataArray[nIndex1][3];
                            break;
                        }
                    }
                }
            }
        	
	    if(g_dataArray != null)
            {
                for(var nIndex = 0; nIndex < g_dataArray.length; nIndex++)
                {
                    delete g_dataArray[nIndex];
                }
                delete g_dataArray;
                g_dataArray = null;
            }
            
	        // submit the form
	        this.AutoSubmitForm(formWrite);
            
            return true;
        }
        catch(ex)
        {
            //console.log("WriteDataToForm failed");
            //console.log(ex);
        }
    }
    
};

var CommonModule = 
{
    // Initialize the string by local language id. 
    InitStringByLangID : function (langID)
    {
        var tempLangID = 1033;
        // 
        if(null != langID)
        {
            if(3076 == langID || 5124 == langID)
		    {
			    tempLangID = 1028;
		    }
		    else if(4100 == langID)
		    {
			    tempLangID = 2052;
		    }
		    else if(1046 == langID)
		    {
		        //tempLangID = 2070; //Portuguese European português (Portugal) pt-PT PTG  0816 (2070) 葡萄牙语(葡萄牙)
		        tempLangID = 1046; //Protuguese Brazilian Português (Brasil) pt-BR PTB  0416 (1046) 葡萄牙语(巴西) 
		    }				
		    else
		    {
			    tempLangID = langID;
		    }
        }
				
        switch(tempLangID)
        {
        case 1028: // Chinese (Traditional) zh-TW
            g_strBtnHelpCaption  = "說明";
            g_strBtnCloseCaption = "關閉"; 
            g_strRegMsg          = "該網站已註冊可使用自動密碼輸入，請劃過您的手指！"; 
            g_strNotRegMsg       = "該網站還未註冊使用自動密碼輸入，請劃過您的手指！";
            break;
        case 1029: // Czech cs-CZ
            g_strBtnHelpCaption  = "Nápověda";
            g_strBtnCloseCaption = "Zavřít"; 
            g_strRegMsg          = "Tato webová stránka byla zaregistrována pro automatické zadávání hesla, dotkněte se prstem!"; 
            g_strNotRegMsg       = "Tato webová stránka nebyla dosud zaregistrována pro automatické zadávání hesla, dotkněte se prstem!";
            break;
        case 1030: // Danish da-DK
            g_strBtnHelpCaption  = "hjælp";
            g_strBtnCloseCaption = "Luk"; 
            g_strRegMsg          = "Denne hjemmeside er registreret til automatisk indtastning af adgangskode. Scan din finger!"; 
            g_strNotRegMsg       = "Denne hjemmeside er endnu ikke registreret til automatisk indtastning af adgangskode. Scan din finger!";
            break;
        case 1031: // German de-DE
            g_strBtnHelpCaption  = "Hilfe";
            g_strBtnCloseCaption = "Schließen"; 
            g_strRegMsg          = "Diese Website wurde zur automatischen Kennworteingabe registriert. Geben Sie Ihren Fingerabdruck ab."; 
            g_strNotRegMsg       = "Diese Website wurde noch nicht zur automatischen Kennworteingabe registriert. Geben Sie Ihren Fingerabdruck ab.";
            break;
        case 1032: // Greek  el-gr
            g_strBtnHelpCaption  = "Βοήθεια";
            g_strBtnCloseCaption = "Κλείσιμο"; 
            g_strRegMsg          = "Αυτή η τοποθεσία web έχει καταχωριστεί για αυτόματη εισαγωγή κωδικού πρόσβασης. Περάστε το δάκτυλό σας πάνω από τον αισθητήρα!"; 
            g_strNotRegMsg       = "Αυτή η τοποθεσία web δεν έχει καταχωριστεί ακόμη για αυτόματη εισαγωγή κωδικού πρόσβασης. Περάστε το δάκτυλό σας πάνω από τον αισθητήρα!";
            break;
        case 1033: // English en-us
            g_strBtnHelpCaption  = "Help";
            g_strBtnCloseCaption = "Close"; 
            g_strRegMsg          = "This website has been registered for automatic password input, please swipe your finger!";
            g_strNotRegMsg       = "This website has not been registered for automatic password input yet, please swipe your finger!";
            break;
        case 1035: // Finnish fi-FI
            g_strBtnHelpCaption  = "ohje";
            g_strBtnCloseCaption = "Sulje"; 
            g_strRegMsg          = "Tämän sivuston salasana on rekisteröity automaattisesti syötettäväksi, anna sormenjälkesi!"; 
            g_strNotRegMsg       = "Tämän sivuston salasanaa ei ole vielä rekisteröity automaattisesti syötettäväksi, anna sormenjälkesi!";
            break;
        case 1036: // French fr-FR
            g_strBtnHelpCaption  = "Aide";
            g_strBtnCloseCaption = "Fermer"; 
            g_strRegMsg          = "Vous avez sélectionné ce site Web pour que la saisie automatique du mot de passe soit appliquée ; faites glisser votre doigt sur le capteur."; 
            g_strNotRegMsg       = "Ce site Web n'a pas encore été enregistré pour que la saisie automatique du mot de passe soit appliquée ; faites glisser votre doigt sur le capteur.";
            break;
        case 1038: // Hungarian hu-HU
            g_strBtnHelpCaption =  "Súgó";
            g_strBtnCloseCaption = "Bezárás"; 
            g_strRegMsg          = "A weboldalra már regisztrálva van az automatikus jelszóbeviteli funkció. Olvastassa be az ujjlenyomatát."; 
            g_strNotRegMsg       = "A weboldalra még nincs regisztrálva az automatikus jelszóbeviteli funkció. Olvastassa be az ujjlenyomatát.";
            break;
        case 1040: // Italian it-IT
            g_strBtnHelpCaption  = "Guida";
            g_strBtnCloseCaption = "Chiudi"; 
            g_strRegMsg          = "Questo sito Web è stato registrato per l'immissione automatica delle password, passare il dito!"; 
            g_strNotRegMsg       = "Questo sito Web non è stato ancora registrato per l'immissione automatica delle password, passare il dito!";
            break;
        case 1041: // Japanese ja-JP
            g_strBtnHelpCaption  = "へルプ";
            g_strBtnCloseCaption = "閉じる";
            g_strRegMsg          = "登録済みのウェブサイトです。指紋をスキャンしてください。";
            g_strNotRegMsg       = "このウェブサイトはまだ登録されていません。登録したい場合は指紋をスキャンしてください。";
            break;
        case 1042: // Korean ko-KR 
            g_strBtnHelpCaption  = "도움말";
            g_strBtnCloseCaption = "닫기"; 
            g_strRegMsg          = "이 웹사이트는 자동 암호 입력에 등록되었습니다. 손가락을 가져다 대십시오!"; 
            g_strNotRegMsg       = "이 웹사이트는 아직 자동 암호 입력에 등록되지 않았습니다. 손가락을 가져다 대십시오!";
            break;
        case 1043: // Dutch nl-NL
            g_strBtnHelpCaption  = "Help";
            g_strBtnCloseCaption = "Sluiten"; 
            g_strRegMsg          = "Deze website is geregistreerd voor automatisch invoeren van wachtwoord, veeg met uw vinger."; 
            g_strNotRegMsg       = "Deze website is nog niet geregistreerd voor automatisch invoeren van wachtwoord, veeg met uw vinger.";
            break;
        case 1044: // Norwegian nb-NO
            g_strBtnHelpCaption  = "Hjelp";
            g_strBtnCloseCaption = "Lukk"; 
            g_strRegMsg          = "Dette webområdet er registrert for automatisk passordangivelse. Før fingeren over føleren."; 
            g_strNotRegMsg       = "Dette webområdet er ikke registrert for automatisk passordangivelse enda. Før fingeren over føleren.";
            break;
        case 1045: // Polish pl-PL
            g_strBtnHelpCaption  = "Pomoc";
            g_strBtnCloseCaption = "Zamknij"; 
            g_strRegMsg          = "Ta strona internetowa została zarejestrowana do automatycznego wprowadzania danych. Przesuń palec nad czytnikiem!"; 
            g_strNotRegMsg       = "Ta strona internetowa nie została jeszcze zarejestrowana do automatycznego wprowadzania danych. Przesuń palec nad czytnikiem!";
            break;
        case 2070: // Portuguese pt-pT
            g_strBtnHelpCaption  = "Ajuda";
            g_strBtnCloseCaption = "Fechar"; 
            g_strRegMsg          = "Este website foi registado para introdução automática da palavra-passe. Passe o dedo!"; 
            g_strNotRegMsg       = "Este website ainda não foi registado para introdução automática da palavra-passe. Passe o dedo!";
            break;
        case 1049: // Russian ru-RU
            g_strBtnHelpCaption  = "Справка";
            g_strBtnCloseCaption = "Закрыть"; 
            g_strRegMsg          = "Этот веб-сайт зарегистрирован для автоматического ввода пароля. Проведите пальцем по поверхности датчика!"; 
            g_strNotRegMsg       = "Этот веб-сайт еще не зарегистрирован для автоматического ввода пароля. Проведите пальцем по поверхности датчика!";
            break;
        case 1051: // Slovak sk-SK
            g_strBtnHelpCaption  = "Pomocník";
            g_strBtnCloseCaption = "Zavrieť"; 
            g_strRegMsg          = "Táto webová stránka bola zaregistrovaná pre automatické vkladanie hesla. Prejdite prstom po senzore."; 
            g_strNotRegMsg       = "Táto webová stránka zatiaľ nebola zaregistrovaná pre automatické vkladanie hesla. Prejdite prstom po senzore.";
            break;
        case 1053: // Swedish sv-SE
            g_strBtnHelpCaption  = "Hjälp";
            g_strBtnCloseCaption = "Stäng"; 
            g_strRegMsg          = "Automatisk inmatning av lösenord har registrerats för den här webbplatsen – läs av ditt finger."; 
            g_strNotRegMsg       = "Automatisk inmatning av lösenord har inte registrerats för den här webbplatsen än – läs av ditt finger.";
            break;
        case 1055: // Turkish tr-TR
            g_strBtnHelpCaption  = "yardım";
            g_strBtnCloseCaption = "Kapat"; 
            g_strRegMsg          = "Bu web sitesi otomatik parola girişi için kaydedilmiş, lütfen parmağınızı geçirin!"; 
            g_strNotRegMsg       = "Bu web sitesi otomatik parola girişi için kaydedilmemiş, lütfen parmağınızı geçirin!";
            break;
        case 2052: // Chinese (Simplified) zh-CN
            g_strBtnHelpCaption  = "帮助";
            g_strBtnCloseCaption = "关闭"; 
            g_strRegMsg          = "该网站已注册可使用自动密码输入，请划过您的手指！"; 
            g_strNotRegMsg       = "该网站还未注册使用自动密码输入，请划过您的手指！";
            break;
        case 3082: // Spanish es-ES
            g_strBtnHelpCaption  = "Ayuda";
            g_strBtnCloseCaption = "Cerrar"; 
            g_strRegMsg          = "Se ha registrado la introducción automática de la palabra clave para este sitio Web. Pase el dedo."; 
            g_strNotRegMsg       = "Aún no se ha registrado la introducción automática de la palabra clave para este sitio Web. Pase el dedo.";
            break;
        case 1046: // Portuguese pt-BR
            g_strBtnHelpCaption  = "Ajuda";
            g_strBtnCloseCaption = "Fechar";
            g_strRegMsg          = "Este website foi registrado para entrada automática de senha, por favor passe o seu dedo no sensor!";
            g_strNotRegMsg       = "Este website ainda não foi registrado para entrada automática de senha, por favor passe o seu dedo no sensor!";
            break;
        default:
            break;
        }
    },
    
    // Get the html string of div element.
    GetDivHtml : function()
    {
        try
        {
           ////console.log("GetDivHtml");
           var strMes  = g_strNotRegMsg;
           var strColor = g_strNotRegBgColor;
           
           if(g_bRegistered)
           {        
                
                strMes  = g_strRegMsg;
                strColor = g_strRegBgColor;
                if (g_strBGColor != "")
                {
					strColor = "#" + g_strBGColor;
                }
           }
           else
           {
                strMes  = g_strNotRegMsg;
                strColor = g_strNotRegBgColor;
                if (g_strBGColor != "")
                {
					strColor = "#" + g_strBGColor;
                }
           }
           
           var strHtml = "";
        	
	        strHtml = strHtml + "<DIV id = '"
	                          + g_strDivID
	                          + "' align=center style =' width:100%; padding:1; height:20px; background:"
	                          + strColor
	                          + "; height:20px; color:"
	                          + g_strFontColor
	                          + "; z-index:10000; FILTER: progid:DXImageTransform.Microsoft.Glow(color="
	                          + g_strBorderColor
	                          + ",strength=5); font-size:12px; font-weight:bold; font-style:normal; font-family:Verdana; '>"
	                          + strMes
	                          + this.GetButtonHtml()
	                          + "</DIV>";
        		   
	        return strHtml; 
        }
        catch(ex)
        {
            //console.log("GetDivHtml");
            //console.log(ex);
        }
    },
    
    // Get the html string of button element.
    GetButtonHtml : function()
    {
        ////console.log("GetButtonHtml");
        var strHtml = "";

	    strHtml = strHtml + "<input align =right type=button value='"
	                      + g_strBtnCloseCaption
	                      + "' onclick = document.body.removeChild(document.getElementById('"
	                      + g_strDivID
	                      + "')) "
	                      + "style ='font-size:11px; font-style:normal; font-family:Verdana; cursor:hand; height:20px; width:70px;' ></input>"
	                      + "<input id='"
	                      + g_strBtnHelpID
	                      + "' align =right type=button value='"
	                      + g_strBtnHelpCaption
	                      + "' style ='font-size:11px; font-style:normal; font-family:Verdana; cursor:hand; height:20px; width:70px;' ></input>";
    	
        return strHtml;
    },
    
    // get the current firefox window
    getBaseWindow : function(win) 
    {
        var rv;
        try
        {
            var requestor = win.QueryInterface(Components.interfaces.nsIInterfaceRequestor);
            var nav       = requestor.getInterface(Components.interfaces.nsIWebNavigation);
            var dsti      = nav.QueryInterface(Components.interfaces.nsIDocShellTreeItem);
            var owner     = dsti.treeOwner;
            requestor     = owner.QueryInterface(Components.interfaces.nsIInterfaceRequestor);
            rv            = requestor.getInterface(Components.interfaces.nsIXULWindow);
            rv            = rv.docShell;
            rv            = rv.QueryInterface(Components.interfaces.nsIDocShell);
            rv            = rv.QueryInterface(Components.interfaces.nsIBaseWindow);
        }
        catch(ex)
        {
            rv = null;
            setTimeout(function(){ throw ex  }, 0);
        }
        
        return rv;
    },
    
    OpenTFPUHelpFile : function()
    {
        try
        {
			chrome.extension.sendRequest("openHelpFile");
		}
        catch(ex)
        {
            //console.log("OpenTFPUHelpFile");
            //console.log(ex);
        }
    }
};